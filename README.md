Short description of the projects in this repository:

Books - is a simple  JavaFX desktop application created with (not only) those foreign language learners in mind who like to learn by reading books. It is a database tool, which allows tracking the status of the books (read, being read, waiting to be read), the number of pages etc. There are also simple reports with charts showing the general progress. A MS Access file is used as a database for the application. The application uses the ucanaccess driver to connect to the database.

Ludo - is a JavaFX game. The equivalent of German "Mensch, �rgere dich nicht!" The rules are described in a window which shows after the game is started. The human player can play against 1 - 3 "AI players".

Grapher - a JavaFX application for drawing graphs of mathematical functions (defined by an expression). Implements the shunting yard algorhythm to evaluate the expression.
