package model;

public class Point {
	
	private double x; 
	private double y;
	private double unitLength;
	
	public Point(double x, double y, double unitLength) {
		this.x = x;
		this.y = y;
		this.unitLength = unitLength;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
	
	/**
	 * 
	 * @return the "x" coordinate of the point in the default coordinates system within the graph pane
	 * @param	chiOfYAxis
	 */
	public double getChi(double chiOfYAxis) {
		return x * unitLength + chiOfYAxis;
	}
	
	/**
	 * 
	 * @param psiOfXAxis
	 * @return the "y" coordinate of the point in the default coordinates system within the graph pane
	 */
	public double getPsi(double psiOfXAxis) {
		return psiOfXAxis - y * unitLength;
	}
	
}
