package model;

public enum TokenType {
	NUMBER, OPERATOR, FUNCTION;
}
