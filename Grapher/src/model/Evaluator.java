package model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Stack;
import java.util.function.Function;

import exceptions.MismatchedParenthesesException;
import exceptions.MisplacedCharacterException;
import exceptions.MissingArgumentException;
import exceptions.OperationUndefinedException;
import exceptions.UnknownFunctionException;

public class Evaluator {

	/**
	 * uses shunting yard algorhythm to convert an expression from standard
	 * algebraic notation to Polish reverse notation
	 * 
	 * @param expression
	 *            the algebraic representation of the function to be evaluated
	 * @param xValue
	 *            the value of variable x for which the function should be evaluated
	 * @return a queue representing the expression in reverse polish notation
	 * @throws MisplacedCharacterException
	 */
	public static List<Token> postFix(String expression)
			throws MismatchedParenthesesException, MisplacedCharacterException {
		List<Token> tokens = tokenize(expression);
		List<Token> results = new LinkedList<>();
		Stack<Token> operators = new Stack<>();

		shuntTokens(tokens, results, operators);
		shuntRemainingOperators(results, operators);

		return results;

	}
	
	/**
	 * evaluates the expression in Reverse Polish Notation (represented by a List)
	 * for a specified value of x.
	 * 
	 * @param postFixedExpression
	 *            list representing the expression in Reverse Polish Notation
	 * @param xValue
	 *            the value for x to substitute in the expression
	 * @return Optional<Double> representing the value of the expression (if the x
	 *         doesn't fall into the range of the function, return empty Optional).
	 * @throws MissingArgumentException
	 * @throws UnknownFunctionException
	 */

	public static Optional<Double> evaluateExpression(List<Token> postFixedExpression, double xValue)
			throws MissingArgumentException, UnknownFunctionException {

		if (postFixedExpression.isEmpty()) {
			return Optional.empty();
		}

		Stack<Double> auxiliary = new Stack<>();

		for (int i = 0; i < postFixedExpression.size(); i++) {
			Token actualToken = postFixedExpression.get(i);
			TokenType actualTokenType = actualToken.getTokenType();
			String actualTokenString = actualToken.getTokenString();

			switch (actualTokenType) {
			case FUNCTION:
				evaluateFunction(auxiliary, actualTokenString);
				break;
			case NUMBER:
				double tokenValue;
				if ("x".equals(actualTokenString)) {
					tokenValue = xValue;
				} else {
					tokenValue = Double.parseDouble(actualTokenString);
				}
				auxiliary.push(tokenValue);
				break;
			case OPERATOR:
				try {
					evaluateOperator(auxiliary, actualTokenString);
				} catch (OperationUndefinedException e) {
					return Optional.empty();
				}
				break;
			}
			if (auxiliary.empty()) {
				throw new MissingArgumentException(actualTokenString);
			}
		}
		return Optional.of(auxiliary.pop());
	}


	/*
	 * Attach any remaining operators at the end of the result list. If the operator
	 * is "(", parentheses don't match.
	 */
	private static void shuntRemainingOperators(List<Token> results, Stack<Token> operators)
			throws MismatchedParenthesesException {
		while (!operators.empty()) {
			if ("(".equals(operators.peek().getTokenString())) {
				throw new MismatchedParenthesesException();
			} else {
				shuntOperatorToResult(results, operators);
			}
		}
	}

	private static void shuntTokens(List<Token> tokens, List<Token> results, Stack<Token> operators)
			throws MismatchedParenthesesException {
		for (Token t : tokens) {

			if (t.getTokenType() == TokenType.NUMBER) {
				results.add(t);
			}

			if (t.getTokenType() == TokenType.FUNCTION) {
				operators.push(t);
			}
			if (t.getTokenType() == TokenType.OPERATOR) {
				if (t.getTokenString().equals("(")) {
					operators.push(t);
				} else if (t.getTokenString().equals(")")) {
					handleRightBracket(results, operators);
				} else {
					handleOperator(results, operators, t);
				}
			}
		}
	}

	private static void handleOperator(List<Token> results, Stack<Token> operators, Token actualToken) {
		if (!operators.empty()) {

			Token topToken = operators.peek();
			TokenType topTokenType = topToken.getTokenType();
			String topOperator = topToken.getTokenString();
			String actualOperator = actualToken.getTokenString();

			/*
			 * shunting operators to result until: a) operator stack is empty or b) we find
			 * a function or c) we find an operator with higher precedence or d) we find a
			 * right bracket
			 */
			while (!operators.empty()
					&& (topTokenType == TokenType.FUNCTION || lowerPrecedence(actualOperator, topOperator))
					&& !topOperator.equals("(")) {
				shuntOperatorToResult(results, operators);
				if (!operators.empty()) {
					topToken = operators.peek(); // set new topToken
				}
			}
		}
		operators.push(actualToken);
	}

	/*
	 * moves the top operator from operatorsStack to results
	 */
	private static void shuntOperatorToResult(List<Token> results, Stack<Token> operators) {
		results.add(operators.pop());
	}

	private static void handleRightBracket(List<Token> results, Stack<Token> operators)
			throws MismatchedParenthesesException {
		if (operators.empty()) {
			throw new MismatchedParenthesesException();
		}
		Token topToken = operators.peek();
		while (!"(".equals(topToken.getTokenString())) {
			shuntOperatorToResult(results, operators);
			if (operators.empty()) {
				throw new MismatchedParenthesesException();
			}
			topToken = operators.peek();
		}
		operators.pop();
	}

	
	private static void evaluateOperator(Stack<Double> auxiliary, String actualTokenString)
			throws MissingArgumentException, OperationUndefinedException {
		if (auxiliary.empty()) {
			throw new MissingArgumentException(actualTokenString);
		}
		double argument2 = auxiliary.pop();

		if (auxiliary.empty()) {
			throw new MissingArgumentException(actualTokenString);
		}

		double argument1 = auxiliary.pop();

		switch (actualTokenString) {
		case "+":
			auxiliary.push(argument1 + argument2);
			break;
		case "-":
			auxiliary.push(argument1 - argument2);
			break;
		case "*":
			auxiliary.push(argument1 * argument2);
			break;
		case "/":
			if (argument2 == 0) {
				throw new OperationUndefinedException();
			}
			auxiliary.push(argument1 / argument2);
			break;
		case "^":
			if (argument1 < 0 && (argument2 % 1 != 0)) {
				throw new OperationUndefinedException();
			}
			auxiliary.push(Math.pow(argument1, argument2));
			break;
		}
	}

	private static void evaluateFunction(Stack<Double> auxiliary, String actualTokenString)
			throws MissingArgumentException, UnknownFunctionException {
		Function<Double, Double> function = parseFunction(actualTokenString);
		if (auxiliary.empty()) {
			throw new MissingArgumentException(actualTokenString);
		}
		double argument = auxiliary.pop();
		auxiliary.push(function.apply(argument));

	}

	private static Function<Double, Double> parseFunction(String functionString) throws UnknownFunctionException {
		switch (functionString) {
		case "sin":
			return x -> Math.sin(x);
		case "cos":
			return x -> Math.cos(x);
		case "tan":
		case "tg":
			return x -> Math.tan(x);
		case "e":
			return x -> Math.exp(x);
		case "abs":
			return x -> Math.abs(x);
		case "arcsin":
			return x -> Math.asin(x);
		case "arccos":
			return x -> Math.acos(x);
		case "arctg":
		case "arctan":
			return x -> Math.atan(x);
		case "sqrt":
			return x -> Math.sqrt(x);
		case "ln":
		case "log":
			return x -> Math.log(x);
		default:
			throw new UnknownFunctionException(functionString);
		}

	}

	public static List<Token> tokenize(String expression) throws MisplacedCharacterException {
		List<Token> tokens = new ArrayList<>();
		int position = 0;
		StringBuilder numberBuilder = new StringBuilder();
		StringBuilder functionBuilder = new StringBuilder();

		while (position < expression.length()) {
			String character = getCharacter(expression, position);

			/*
			 * if character is the minus sign and it's on the first position or after a left
			 * bracker, then it is a negative sign. Insert zero before it to make it a
			 * substraction operation.
			 */
			if ("-".equals(character)
					&& (position == 0 || String.valueOf(expression.charAt(position - 1)).equals("("))) {
				createToken(tokens, "0", TokenType.NUMBER);
			}

			/*
			 * if the character is a number, a dot or an alphabeth sign, read for as long as
			 * the character is of the same type and then concatenate to a number or a
			 * function
			 * 
			 */

			while (character.matches("[0-9]|\\.")) {
				numberBuilder.append(character);
				position++;
				character = getCharacter(expression, position);
			}
			if (!builderIsEmpty(numberBuilder)) {
				createToken(tokens, numberBuilder, TokenType.NUMBER);
				numberBuilder = new StringBuilder();
			}
			while (character.matches("[a-w|y-z]")) {
				functionBuilder.append(character);
				position++;
				character = getCharacter(expression, position);
			}
			if (!builderIsEmpty(functionBuilder)) {
				createToken(tokens, functionBuilder, TokenType.FUNCTION);
				functionBuilder = new StringBuilder();
			}

			if (!"".equals(character)) { // if we are not at the end of the expression
				/*
				 * after a function or a number (created in previous while loops) an operator,
				 * an "x" or a bracket should follow
				 */

				if ("x".equals(character)) {
					if (position != expression.length() - 1
							&& !String.valueOf(expression.charAt(position + 1)).matches("\\+|\\-|\\*|\\/|\\^|\\)")) {
						throw new MisplacedCharacterException(
								"If the variable x doesn't appear at the end of the expression,"
										+ "it can only be followed by an operator or a right bracket");
					}
					if (position != 0
							&& !String.valueOf(expression.charAt(position - 1)).matches("\\+|\\-|\\*|\\/|\\^|\\(")) {
						throw new MisplacedCharacterException(
								"If the variable x doesn't appear at the beginning of the expression,"
										+ "it can only be preceded by an operator or a left bracket.");
					}
					createToken(tokens, character, TokenType.NUMBER);
				} else if (character.matches("\\+|\\-|\\*|\\/|\\^|\\)")) {
					createToken(tokens, character, TokenType.OPERATOR);
				} else if ("(".equals(character)) {
					createToken(tokens, character, TokenType.OPERATOR);
				} else {
					throw new MisplacedCharacterException(
							"The character " + character + " appears in an unexpected position.");
				}
				position++;
				character = getCharacter(expression, position);
			}
		}
		return tokens;
	}

	private static String getCharacter(String expression, int position) {
		if (position < expression.length()) {
			return String.valueOf(expression.charAt(position));
		} else {
			return "";
		}

	}

	private static void createToken(List<Token> tokens, String tokenString, TokenType tokenType) {
		tokens.add(new Token(tokenType, tokenString));
	}

	private static void createToken(List<Token> tokens, StringBuilder tokenBuilder, TokenType tokenType) {
		createToken(tokens, tokenBuilder.toString(), tokenType);
	}

	private static boolean builderIsEmpty(StringBuilder builder) {
		return builder.toString() == null || builder.toString().length() <= 0;
	}

	// returns true if operator1 has lower precedence than operator2
	private static boolean lowerPrecedence(String operator1, String operator2) {
		return (operator1.matches("\\+|\\-")) || ((operator1.matches("\\*|\\/") && operator2.matches("\\^")));
	}

}
