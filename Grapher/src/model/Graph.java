package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import exceptions.MismatchedParenthesesException;
import exceptions.MisplacedCharacterException;
import exceptions.MissingArgumentException;
import exceptions.UnknownFunctionException;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.text.Text;

/*
 * This class represents the function plot together with the axes, labels and the raster.
 * The plot itself is represented by a list of straight lines connecting the valid (x,y)-points of the plot.
 */

public class Graph {
	private Pane graphPane;
	private double width;
	private double height;

	private String expression;
	private List<Line> plotLines;

	private double chiOfYAxis;
	private double psiOfXAxis;

	private double unitLength;
	private double fidelity;
	private Paint color;

	/**
	 * 
	 * @param expression
	 *            the mathematical representation of the function
	 * @param unitLength
	 *            the distance from 0 to 1 on x and y axis in pixels
	 * @param chiOfYAxis
	 *            the position of the y axis within the graphPane, chi is it's "x"
	 *            coordinate
	 * @param psiOfXAxis
	 *            the position of the x axis within the graphPane, psi is it's "y"
	 *            coordinate
	 * @param fidelity
	 *            the step between x points which are sent as arguments to the
	 *            function
	 * @param graphPane
	 * @param color
	 *            the color of the function plot
	 */
	public Graph(String expression, double unitLength, double chiOfYAxis, double psiOfXAxis, double fidelity,
			Pane graphPane, Paint color) {
		this.expression = expression;
		this.graphPane = graphPane;
		this.unitLength = unitLength;
		this.fidelity = fidelity;
		this.color = color;
		this.psiOfXAxis = psiOfXAxis;
		this.chiOfYAxis = chiOfYAxis;

		height = graphPane.getMaxHeight();
		width = graphPane.getMaxWidth();

		drawGraphPane();
	}

	private void drawPlot() throws MissingArgumentException, UnknownFunctionException, MismatchedParenthesesException,
			MisplacedCharacterException {

		double startX = -chiOfYAxis / unitLength; // the lower limit of the function arguments (to fit within the pane)
		double endX = startX + width / unitLength; // the upper limit of the function arguments (to fit within the pane)

		plotLines = new ArrayList<>();
		Optional<Point> oPreviousPoint = Optional.empty();
		List<Token> postFixedExpression = Evaluator.postFix(expression); // reading the expression and converting it to
																			// reverse polish notation

		for (double x = startX; x < endX; x += fidelity) {
			Optional<Point> oActualPoint;
			Optional<Double> optionalY = Evaluator.evaluateExpression(postFixedExpression, x); // evaluate the expression for x
			if (optionalY.isPresent()) {
				double y = optionalY.get();
				oActualPoint = Optional.of(new Point(x, y, unitLength));
			} else {
				oActualPoint = Optional.empty();
			}
			if (isValid(oActualPoint)) {
				Point actualPoint = oActualPoint.get();
				if (oPreviousPoint.isPresent()) {
					Point previousPoint = oPreviousPoint.get();
					drawPlotLine(actualPoint, previousPoint);
				}
				oPreviousPoint = oActualPoint;
			} else {
				oPreviousPoint = Optional.empty();
			}
		}
		graphPane.getChildren().addAll(plotLines);
	}

	private void drawPlotLine(Point actualPoint, Point previousPoint) {
		Line plotLine = new Line(previousPoint.getChi(chiOfYAxis), previousPoint.getPsi(psiOfXAxis),
				actualPoint.getChi(chiOfYAxis), actualPoint.getPsi(psiOfXAxis));
		plotLine.setStroke(color);
		plotLines.add(plotLine);
	}

	/*
	 * checks whether the point is present and if so, then chek wether it fits
	 * within the height of the pane. We don't have to check the width of the pane,
	 * because we only execute calculations of the plot within range of x from
	 * startX to endX, i.e. in the range of chi from 0 to width.
	 */
	private boolean isValid(Optional<Point> oPoint) {
		if (oPoint.isPresent()) {
			Point point = oPoint.get();
			return point.getPsi(psiOfXAxis) < height && point.getPsi(psiOfXAxis) > 0;
		}
		return false;
	}

	private void drawGraphPane() {
		try {
			clearGraphPane();
			drawPlot();
			drawAxes();
			drawRaster();
		} catch (MissingArgumentException | UnknownFunctionException | MismatchedParenthesesException
				| MisplacedCharacterException e) {
		}
	}

	private void clearGraphPane() {
		graphPane.getChildren().clear();
	}

	private void drawRaster() {
		double rasterChi = chiOfYAxis - unitLength;
		while (rasterChi > 0) {
			showTickLabelsOnXAxis(rasterChi);
			drawYRasterLine(rasterChi);
			rasterChi -= unitLength;
		}
		rasterChi = chiOfYAxis + unitLength;
		while (rasterChi < width) {
			showTickLabelsOnXAxis(rasterChi);
			drawYRasterLine(rasterChi);
			rasterChi += unitLength;
		}
		double rasterPsi = psiOfXAxis - unitLength;
		while (rasterPsi > 0) {
			showTickLabelsOnYAxis(rasterPsi);
			drawXRasterLine(rasterPsi);
			rasterPsi -= unitLength;
		}
		rasterPsi = psiOfXAxis + unitLength;
		while (rasterPsi < height) {
			showTickLabelsOnYAxis(rasterPsi);
			drawXRasterLine(rasterPsi);
			rasterPsi += unitLength;
		}
	}

	private void drawXRasterLine(double rasterPsi) {
		if (rasterPsi > 0 && rasterPsi < height) {
			Line rasterLine = new Line(0, rasterPsi, width, rasterPsi);
			setRasterLineStyle(rasterLine);
			graphPane.getChildren().addAll(rasterLine);
		}
	}

	private void drawYRasterLine(double rasterChi) {
		if (rasterChi > 0 && rasterChi < width) {
			Line rasterLine = new Line(rasterChi, 0, rasterChi, height);
			setRasterLineStyle(rasterLine);
			graphPane.getChildren().addAll(rasterLine);
		}
	}

	private void setRasterLineStyle(Line rasterLine) {
		rasterLine.setStrokeWidth(1);
		rasterLine.getStrokeDashArray().addAll(1d, 3d);
		rasterLine.setStrokeLineCap(StrokeLineCap.ROUND);
	}

	private void showTickLabelsOnYAxis(double psi) {
		double margin = 15;
		double chiOffset = 5;
		double psiOffset = 15;
		/* check wether the label fits on the graphPane */
		if (chiOfYAxis > 0 && chiOfYAxis < width - margin && psi > 0 && psi < height - margin) {
			long tickValue = Math.round((psiOfXAxis - psi) / unitLength);
			Text tickLabelText = new Text(chiOfYAxis + chiOffset, psi + psiOffset, String.valueOf(tickValue));
			graphPane.getChildren().add(tickLabelText);
		}
	}

	private void showTickLabelsOnXAxis(double chi) {
		double margin = 15;
		double chiOffset = 5;
		double psiOffset = 15;
		if (psiOfXAxis > 0 && psiOfXAxis < height - margin && chi > 0 && chi < width - margin) { // if the number fits
																									// on the graph
			// pane
			long tickValue = Math.round((chi - chiOfYAxis) / unitLength);
			Text tickLabelText = new Text(chi + chiOffset, psiOfXAxis + psiOffset, String.valueOf(tickValue));
			graphPane.getChildren().add(tickLabelText);
		}
	}

	private void drawAxes() {
		if (chiOfYAxis > 0 && chiOfYAxis < width) {
			Line yAxis = new Line(chiOfYAxis, 0, chiOfYAxis, height);
			graphPane.getChildren().add(yAxis);

			drawOriginLabel();
			/* draw arrow on y axis */
			double arrowWidth = 10;
			double arrowLength = 10;
			Polygon triangle = new Polygon(chiOfYAxis, 0, chiOfYAxis - arrowWidth / 2, arrowLength,
					chiOfYAxis + arrowWidth / 2, arrowLength);
			graphPane.getChildren().add(triangle);
		}
		if (psiOfXAxis > 0 && psiOfXAxis < height) {
			Line xAxis = new Line(0, psiOfXAxis, width, psiOfXAxis);
			graphPane.getChildren().add(xAxis);

			/* draw arrow on x axis */
			double arrowWidth = 10;
			double arrowLength = 10;
			Polygon triangle = new Polygon(width, psiOfXAxis, width - arrowLength, psiOfXAxis - arrowWidth / 2,
					width - arrowLength, psiOfXAxis + arrowWidth / 2);
			graphPane.getChildren().add(triangle);
		}
	}

	private void drawOriginLabel() {
		if (chiOfYAxis + 15 < width && psiOfXAxis > 0 && psiOfXAxis < height - 15) {
			Text valueText = new Text(chiOfYAxis + 5, psiOfXAxis + 15, "0");
			graphPane.getChildren().add(valueText);
		}
	}

	public void changeUnitLength(double newUnitLength) {
		this.unitLength = newUnitLength;
		drawGraphPane();
	}

	public void changeFidelity(double fidelity) {
		this.fidelity = fidelity;
		drawGraphPane();
	}

	public double getUnitLength() {
		return unitLength;
	}

	public void changeChiOfYAxis(double newChi) {
		this.chiOfYAxis = newChi;
		drawGraphPane();
	}

	public void changePsiOfXAxis(double newPsi) {
		this.psiOfXAxis = newPsi;
		drawGraphPane();
	}

	public double getChiOfYAxis() {
		return chiOfYAxis;
	}

	public double getPsiOfXAxis() {
		return psiOfXAxis;
	}

}
