package model;

public class Token {

	private TokenType tokenType;
	private String tokenString;

	public Token(TokenType tokenType, String tokenString) {
			this.tokenType = tokenType;
			this.tokenString = tokenString;
		}

	public TokenType getTokenType() {
		return tokenType;
	}

	public String getTokenString() {
		return tokenString;
	}
	public String toString() {
		return tokenType + ": " + tokenString;
	}
}
