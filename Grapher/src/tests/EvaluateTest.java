package tests;

import java.util.List;
import java.util.Scanner;

import exceptions.MismatchedParenthesesException;
import exceptions.MisplacedCharacterException;
import exceptions.MissingArgumentException;
import exceptions.UnknownFunctionException;
import model.Evaluator;
import model.Token;

public class EvaluateTest {
	public static void main(String[] args)
			throws MismatchedParenthesesException, MissingArgumentException, MisplacedCharacterException, UnknownFunctionException {
		Scanner scanner;
		while (true) {
			scanner = new Scanner(System.in);
			System.out.print("Expression: ");
			String expression = scanner.nextLine();
			if ("exit".equals(expression)) {
				break;
			}
			System.out.println("x: ");
			double x = scanner.nextDouble();
			scanner.nextLine();
			List<Token> tokens = Evaluator.tokenize(expression);
			tokens.stream().forEach(System.out::println);
			List<Token> postFixedExpression = Evaluator.postFix(expression);
			System.out.println(Evaluator.evaluateExpression(postFixedExpression, x));
		}
		scanner.close();
	}

}
