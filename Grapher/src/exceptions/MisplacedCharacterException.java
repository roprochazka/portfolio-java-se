package exceptions;

import view.AlertCreator;

public class MisplacedCharacterException extends Exception {
	
	private static final long serialVersionUID = -2952605054116766023L;

	public MisplacedCharacterException(String description) {
		AlertCreator.showAlert("Misplaced character!\n" + description);
	}

}
