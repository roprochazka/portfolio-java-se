package exceptions;

import view.AlertCreator;

public class MissingArgumentException extends Exception {
	
	private static final long serialVersionUID = -5931165891498271847L;

	public MissingArgumentException(String operation) {
		AlertCreator.showAlert("Missing Argument!\nFunction/Operation " + operation + " doesn't have enough arguments.");
	}

}
