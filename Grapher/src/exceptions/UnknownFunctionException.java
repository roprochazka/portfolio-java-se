package exceptions;

import view.AlertCreator;

public class UnknownFunctionException extends Exception {
	
	private static final long serialVersionUID = -9035199544911088631L;

	public UnknownFunctionException(String function) {
		AlertCreator.showAlert("Unknown Function!\nThe function " + function + " is not defined.");
	}
}
