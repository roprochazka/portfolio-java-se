package exceptions;

import view.AlertCreator;

public class MismatchedParenthesesException extends Exception {
	
	private static final long serialVersionUID = -3440448718196989843L;

	public MismatchedParenthesesException() {
		AlertCreator.showAlert("Mismatched parentheses!");
	}

}
