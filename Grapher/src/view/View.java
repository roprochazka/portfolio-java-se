package view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * 
 * The view of the application.
 *
 */

public class View {

	private static final Insets PADDING = new Insets(5);
	private VBox mainFrame;
	private Pane graphPane;
	private GridPane controls;
	private TextField expressionTF;
	private Slider unitLengthSlider;
	private Slider fidelitySlider;
	private Button applyButton;

	public View() {
		createMainFrame();
		createGraphPane();
		createControls();
		mainFrame.getChildren().addAll(graphPane, controls);
	}

	private void createControls() {
		controls = new GridPane();

		Label expressionLabel = new Label("Enter function: y=");
		expressionTF = new TextField();
		expressionTF.setPrefWidth(120);

		Label unitLengthLabel = new Label("   Unit length: ");
		unitLengthSlider = new Slider();
		unitLengthSlider.setMin(25);
		unitLengthSlider.setMax(500);
		unitLengthSlider.setPrefWidth(250);
		unitLengthSlider.setShowTickLabels(true);
		unitLengthSlider.setShowTickMarks(true);
		unitLengthSlider.setValue(100);

		Label fidelityLabel = new Label("   Fidelity: ");
		fidelitySlider = new Slider();
		fidelitySlider.setMin(0);
		fidelitySlider.setMax(500);
		fidelitySlider.setPrefWidth(250);
		fidelitySlider.setShowTickLabels(true);
		fidelitySlider.setShowTickMarks(true);
		fidelitySlider.setMajorTickUnit(25);
		fidelitySlider.setValue(100);

		applyButton = new Button("Apply");
		applyButton.setPrefSize(120, 30);

		controls.add(expressionLabel, 0, 0);
		controls.add(expressionTF, 1, 0);
		controls.add(applyButton, 1, 1);
		controls.add(unitLengthLabel, 2, 0);
		controls.add(unitLengthSlider, 3, 0);
		controls.add(fidelityLabel, 2, 1);
		controls.add(fidelitySlider, 3, 1);

		controls.setHgap(5);
		controls.setVgap(5);
		controls.setAlignment(Pos.CENTER);
		controls.setPadding(PADDING);

	}

	private void createGraphPane() {
		final Color borderColor = Color.BLACK;
		final BorderStrokeStyle borderStrokeStyle = BorderStrokeStyle.SOLID;
		final BorderWidths borderWidths = new BorderWidths(2);
		final Color backgroundColor = Color.ANTIQUEWHITE;
		final double maxWidth = 700;
		final double maxHeight = 600;

		graphPane = new Pane();
		graphPane.setMaxSize(maxWidth, maxHeight);
		graphPane.setPrefSize(maxWidth, maxHeight);
		graphPane.setBorder(new Border(new BorderStroke(borderColor, borderStrokeStyle, null, borderWidths)));
		graphPane.setBackground(new Background(new BackgroundFill(backgroundColor, null, null)));
	}

	private void createMainFrame() {
		mainFrame = new VBox();
		mainFrame.setPadding(PADDING);
		mainFrame.setAlignment(Pos.CENTER);
	}

	public Pane getGraphPane() {
		return graphPane;
	}

	public TextField getExpressionTF() {
		return expressionTF;
	}

	public Slider getUnitLengthSlider() {
		return unitLengthSlider;
	}

	public Button getApplyButton() {
		return applyButton;
	}

	public VBox getMainFrame() {
		return mainFrame;
	}

	public Slider getFidelitySlider() {
		return fidelitySlider;
	}

}
