package view;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class AlertCreator {

	public static void showAlert(String alertText) {
		Alert alert = new Alert(AlertType.NONE, alertText, ButtonType.OK);
		Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
		stage.getIcons().add(new Image("icon.png"));
		alert.setTitle("Warning");
		ImageView alertImage = new ImageView("exclamation.png");
		alert.setGraphic(alertImage);
		alert.showAndWait();
	}

}
