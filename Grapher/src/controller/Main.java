package controller;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import view.View;

public class Main extends Application {
	
	@Override
	public void start(Stage primaryStage) {
		try {
			View view = new View();	// setting up the UI
			new MainController(view);  // setting up the control
			
			Scene scene = new Scene(view.getMainFrame());
			primaryStage.getIcons().add(new Image("icon.png"));
			primaryStage.setTitle("Graphs");
			primaryStage.setResizable(false);
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {

		launch(args);

	}

}
