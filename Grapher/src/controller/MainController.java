package controller;

import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import model.Graph;
import view.View;

/**
 * 
 * The main controller is responsible for the actions connected with events on
 * the UI controls.
 *
 */

public class MainController {

	private static final Color GRAPH_COLOR = Color.BLUE;

	private Graph graph;

	private Pane graphPane;
	private TextField expressionTF;
	private Slider unitLengthSlider;
	private Slider fidelitySlider;
	private Button applyButton;

	private double xDistanceFromYAxis;
	private double yDistanceFromXAxis;

	private double width;
	private double height;

	private double chiOfYAxis;
	private double psiOfXAxis;

	public MainController(View view) {

		/* get UI controls from the view */
		graphPane = view.getGraphPane();
		expressionTF = view.getExpressionTF();
		unitLengthSlider = view.getUnitLengthSlider();
		fidelitySlider = view.getFidelitySlider();
		applyButton = view.getApplyButton();

		width = graphPane.getMaxWidth();
		height = graphPane.getMaxHeight();

		/* initialize the origin of the graph */

		chiOfYAxis = width / 2;
		psiOfXAxis = height / 2;

		createNewGraph();
		setActions();
	}

	/** sets the listeners to the controls */
	private void setActions() {

		unitLengthSlider.valueProperty().addListener(e -> {
			double unitLength = unitLengthSlider.getValue();
			graph.changeUnitLength(unitLength);
		});

		fidelitySlider.valueProperty().addListener(e -> {
			double fidelity = 1 / (fidelitySlider.getValue()+1);
			graph.changeFidelity(fidelity);
		});

		applyButton.setOnAction(x -> {
			createNewGraph();
		});

		graphPane.setOnMousePressed(e -> {
			double mouseX = e.getX();
			double mouseY = e.getY();
			double xOfYAxis = graph.getChiOfYAxis();
			double yOfXAxis = graph.getPsiOfXAxis();
			xDistanceFromYAxis = mouseX - xOfYAxis;
			yDistanceFromXAxis = mouseY - yOfXAxis;

		});

		graphPane.setOnMouseDragged(e -> {
			double mouseChi = e.getX();
			double mousePsi = e.getY();
			if (mouseChi > 0 && mouseChi < width && mousePsi > 0 && mousePsi < height) { // if the mouse is in the graph
																							// pane
				chiOfYAxis = mouseChi - xDistanceFromYAxis;
				psiOfXAxis = mousePsi - yDistanceFromXAxis;
				graph.changeChiOfYAxis(chiOfYAxis);
				graph.changePsiOfXAxis(psiOfXAxis);
			}
		});

		graphPane.setOnScroll(s -> {
			double unitLength = graph.getUnitLength();
			double changeOfUnitScale = s.getDeltaY() / 10;
			double newUnitScale = unitLength + changeOfUnitScale;

			double max = unitLengthSlider.getMax();
			if (newUnitScale > max) {
				newUnitScale = max;
			}
			double min = unitLengthSlider.getMin();
			if (newUnitScale < min) {
				newUnitScale = min;
			}
			graph.changeUnitLength(newUnitScale);
			unitLengthSlider.setValue(newUnitScale);
		});

		expressionTF.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ENTER) {
				createNewGraph();
			}
			if (e.getCode() == KeyCode.ESCAPE) {
				Platform.exit();
			}
		});
	}

	private void createNewGraph() {
		String expression = expressionTF.getText();
		double unitLength = unitLengthSlider.getValue();
		double fidelity = 1 / (fidelitySlider.getValue() + 1);

		graph = new Graph(expression, unitLength, chiOfYAxis, psiOfXAxis, fidelity, graphPane, GRAPH_COLOR);
	}

}
