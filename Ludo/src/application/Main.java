package application;

import controller.GameController;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import view.BoardPainter;
import view.DicePainter;
import view.SettingsManager;
import view.Welcome;

public class Main extends Application {
	@Override
	public void start(Stage stage) {
		try {
			
			Welcome.showRules();
			
			// get parameters (players number, pawns number, human players color)
			SettingsManager settings = new SettingsManager();
			settings.askForSettings();
			
			// prepare the view:
			BoardPainter boardPainter = new BoardPainter();
			boardPainter.initializeBoard(settings); // board must be initilized before we initialize players
			GridPane mainBoardFrame = boardPainter.getMainBoardFrame();
			
			DicePainter dicePainter = new DicePainter();
			Pane diceFrame = dicePainter.getDiceFrame();
			
			// prepare the controller and start the game
			GameController boardController = new GameController(boardPainter, dicePainter, settings);
			boardController.initializePlayers(settings);
			boardController.startGame(settings);
			
			VBox root = new VBox();
			root.setAlignment(Pos.CENTER);
			root.getChildren().addAll(mainBoardFrame, diceFrame);
			
			stage.setResizable(false);
			stage.getIcons().add(new Image("icon.jpg"));
			stage.setTitle("Ludo");
			stage.setScene(new Scene(root));
			stage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
