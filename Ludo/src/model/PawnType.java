package model;

import java.util.Optional;

import javafx.scene.paint.Color;

public enum PawnType {
	BLUE(Color.BLUE, 0),
	RED(Color.RED, 10),
	GREEN(Color.GREEN, 20),
	BLACK(Color.BLACK, 30);
	
	private Color color;
	private int entranceIndex;
	
	private PawnType(Color color, int startingPosition) {
		this.color = color;
		this.entranceIndex = startingPosition;
	}
	
	public static Optional<PawnType> getFromColor(Color color) {
		for (PawnType pawnType:PawnType.values()) {
			if (pawnType.getColor() == color) {
				return Optional.of(pawnType);
			}
		}
		return Optional.empty();
	}
	
	public Color getColor() {
		return color;
	}
	
	public int getEntranceIndex() {
		return entranceIndex;
	}
	
	public int getExitIndex() {
		return entranceIndex == 0 ? Board.PLAYING_FIELDS_NUMBER - 1 : entranceIndex - 1;
	}

}
