package model;

import java.util.Map;
import java.util.Optional;

import javafx.animation.PauseTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import javafx.scene.shape.Shape;
import javafx.util.Duration;
import view.SettingsManager;

public class AIPlayer extends Player {

	public AIPlayer(PawnType pawnType, SettingsManager settings) {
		super(pawnType, settings);
	}

	@Override
	protected void choosePawnAndMove(Map<Integer, Shape> drawnPawns, ObservableMap<Integer, Pawn> pawns, Dice dice, SettingsManager settings) {
		int score = -10000; // set score as low as possible
		Optional<Pawn> optionalChosenPawn = Optional.empty();
		for (int index : pawns.keySet()) {
			Pawn actualPawn = pawns.get(index);
			if (isMoveable(actualPawn, dice, settings)) {
				// creating a map of copies of (test)pawns in the observable map and
				ObservableMap<Integer, Pawn> testPawns = FXCollections.observableHashMap();
				pawns.forEach((i, p) -> {
					Pawn testPawn = new Pawn(pawnType, i, settings);
					testPawns.put(i, testPawn);
				});
				Pawn copyOfActualPawn = testPawns.get(index);
				movePawn(copyOfActualPawn, dice.getValue(), testPawns); // moving the copy of the actual pawn (using the
																	// player's method in order to be able to simulate
																	// throwing out of pawns)
				int testScore = getScore(testPawns); // calculating it's score
				if (testScore > score) { // if the score is better than the actual best score, replace pawn to move
					optionalChosenPawn = Optional.of(actualPawn);
					score = testScore;
				}
			}
		}

		if (optionalChosenPawn.isPresent()) {
			Pawn chosenPawn = optionalChosenPawn.get();
			moveChosenPawn(chosenPawn, dice, pawns);
		}

	}

	/**
	 * 
	 * @param pawns
	 * @return the score of the situation on the board: the sum of the distances
	 *         from the exit for enemy pawns minus the sum of the distances from the
	 *         exit for own pawns. Maximizing the score means better situation for
	 *         the player.
	 */
	private int getScore(Map<Integer, Pawn> pawns) {
		int score = 0;
		for (int k : pawns.keySet()) {
			Pawn pawn = pawns.get(k);
			if (pawn.getPawnType() == pawnType) {
				score -= pawn.getDistanceToExit();
			} else {
				score += pawn.getDistanceToExit();
			}
		}
		return score;
	}

	private void moveChosenPawn(Pawn chosenPawn, Dice dice, ObservableMap<Integer, Pawn> pawns) {
		PauseTransition wait = new PauseTransition(Duration.seconds(1.5)); // wait before finishing turn
		wait.setOnFinished((e) -> {
			movePawn(chosenPawn, dice.getValue(), pawns);
			checkIfWon();
			finishTurn();
		});
		wait.setCycleCount(1);
		wait.play();

	}

}
