package model;

import javafx.scene.paint.Color;
import view.SettingsManager;

public class Pawn {
	private int index;
	private int entranceIndex;
	private int exitIndex;
	private PawnType pawnType;
	private int numberOfPawns;
	private int maxIndex = Board.PLAYING_FIELDS_NUMBER;
	private int startIndex;

	public Pawn(PawnType pawnType, int startIndex, SettingsManager settings) {
		this.startIndex = startIndex; // remember the starting position of the pawn
		index = startIndex;
		this.pawnType = pawnType;
		this.entranceIndex = pawnType.getEntranceIndex();
		this.exitIndex = pawnType.getExitIndex();
		this.numberOfPawns = settings.getPawnsPerPlayer();
	}

	public int getNextIndex() {
		if (index < 0) { // negative indexes are used for starting positions
			return entranceIndex;
		}
		if (index == exitIndex) { // ending positions are 100 + entranceIndex + i
			return 100 + entranceIndex;
		}
		if (index == 100 + entranceIndex + numberOfPawns - 1 || index == 1000) {
			return 1000; // return 1000 if at the end of the path through the board
		}
		if (index + 1 == maxIndex) { // circular fields
			return 0;
		}
		return index + 1;
	}

	public int getDistanceToExit() {
		if (index < 0)
			return 40;
		if (index >= 100)
			return 0;
		return entranceIndex + Board.PLAYING_FIELDS_NUMBER - index;
	}

	public void move(int jumpLength) {
		for (int i = 0; i < jumpLength; i++) {
			this.index = getNextIndex();
		}
	}

	public void returnToStart() {
		this.index = startIndex;
	}
	
	public boolean isAtStart() {
		return index < 0;
	}
	
	public boolean isAtEntrance() {
		return index == entranceIndex;
	}
	
	public boolean isInPlay() {
		return index > 0 && index < 100; 
	}
	
	public boolean isOutOfBoard() {
		return index == 1000;
	}

	public int getIndex() {
		return index;
	}

	public Color getColor() {
		return pawnType.getColor();
	}

	public PawnType getPawnType() {
		return pawnType;
	}
}
