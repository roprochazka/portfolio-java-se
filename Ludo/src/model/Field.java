package model;

public class Field {
	private int columnIndex;
	private int rowIndex;
	private int index;

	public Field(int columnIndex, int rowIndex, int index) {
		this.columnIndex = columnIndex;
		this.rowIndex = rowIndex;
		this.index = index;
	}

	public int getColumnIndex() {
		return columnIndex;
	}

	public int getRowIndex() {
		return rowIndex;
	}

	public int getIndex() {
		return index;
	}

}
