package model;

import java.util.HashMap;
import java.util.Map;

/**
 * Board models the set of fields on the board and is responsible for
 * calculating the column and the row number in the board grid from the index of the
 * field. Assumption: there are 40 playing fields in the game and maximum four
 * pawns per squad.
 */

public class Board {
	public final static int PLAYING_FIELDS_NUMBER = 40; // do not change without changing the conversionMatrix
	
	// a map of Fields used to convert index to coordinates and vice versa
	private Map<Integer, Field> fields;

	// 55 in the matrix means an empty positions
	public static int[][] conversionMatrix = { { -1, -2, 55, 55, 8, 9, 10, 55, 55, -13, -11 },
			{ -3, -4, 55, 55, 7, 110, 11, 55, 55, -14, -12 }, { 55, 55, 55, 55, 6, 111, 12, 55, 55, 55, 55 },
			{ 55, 55, 55, 55, 5, 112, 13, 55, 55, 55, 55 }, { 0, 1, 2, 3, 4, 113, 14, 15, 16, 17, 18 },
			{ 39, 100, 101, 102, 103, 55, 123, 122, 121, 120, 19 }, { 38, 37, 36, 35, 34, 133, 24, 23, 22, 21, 20 },
			{ 55, 55, 55, 55, 33, 132, 25, 55, 55, 55, 55 }, { 55, 55, 55, 55, 32, 131, 26, 55, 55, 55, 55 },
			{ -32, -34, 55, 55, 31, 130, 27, 55, 55, -24, -23 }, { -31, -33, 55, 55, 30, 29, 28, 55, 55, -22, -21 } };
	/* in the converting matrix, first index is a row, the second is a column */

	public Board() {
		fields = new HashMap<>();
		for (int i = 0; i < 11; i++) {
			for (int j = 0; j < 11; j++) {
				int actualIndex = conversionMatrix[i][j];
				if (actualIndex != 55) { // omitting empty fields. Can't put duplicate values to map
					fields.put(actualIndex, new Field(j, i, actualIndex));
				}
				;
			}
		}
	}

	
	 public int getColumnFromIndex(int fieldIndex) {
		 return fields.get(fieldIndex).getColumnIndex();
	 }
	
	 public int getRowFromIndex(int fieldIndex) {
		 return fields.get(fieldIndex).getRowIndex();
	 }
}
