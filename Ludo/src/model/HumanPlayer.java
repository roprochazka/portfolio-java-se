package model;

import java.util.Map;

import javafx.collections.ObservableMap;
import javafx.scene.Cursor;
import javafx.scene.shape.Shape;
import view.SettingsManager;

public class HumanPlayer extends Player {

	public HumanPlayer(PawnType pawnType, SettingsManager settings) {
		super(pawnType, settings);
	}

	protected void choosePawnAndMove(Map<Integer, Shape> drawnPawns, ObservableMap<Integer, Pawn> pawns, Dice dice, SettingsManager settings) {
		for (int index : pawns.keySet()) {
			Pawn pawn = pawns.get(index);
			if (isMoveable(pawn, dice, settings)) {
				registerMoveListener(drawnPawns, pawns, dice, index, pawn);
			}
		}
	}

	private void registerMoveListener(Map<Integer, Shape> drawnPawns, ObservableMap<Integer, Pawn> pawns, Dice dice,
			int index, Pawn pawn) {
		Shape drawnPawn = drawnPawns.get(index);
		// set effect:

		drawnPawn.setRotate(20);

		// set cursor to hand:
		drawnPawn.setOnMouseEntered(e -> drawnPawn.getScene().setCursor(Cursor.HAND));
		drawnPawn.setOnMouseExited(e -> drawnPawn.getScene().setCursor(Cursor.DEFAULT));
		drawnPawn.setOnMouseClicked(e -> {
			movePawn(pawn, dice.getValue(), pawns);
			checkIfWon();
			unregisterMoveListeners(drawnPawns, pawns);
			finishTurn();
		});
	}

	private void unregisterMoveListeners(Map<Integer, Shape> drawnPawns, ObservableMap<Integer, Pawn> pawns) {
		for (int index : pawns.keySet()) {
			if (pawns.get(index).getPawnType() == pawnType && isProcessingTurn.get()) {
				unregisterMoveListener(drawnPawns, index);
			}
		}

	}

	private void unregisterMoveListener(Map<Integer, Shape> drawnPawns, int index) {
		Shape drawnPawn = drawnPawns.get(index);
		drawnPawn.setOnMouseClicked(null); // clicking desactivated
		drawnPawn.setOnMouseEntered(null); // cursor change desactivated
		drawnPawn.setOnMouseExited(null);
		drawnPawn.setRotate(0);
	}

}
