package model;

import java.util.Random;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

public class Dice {
	public final static int DICE_MAX = 6;  
	private IntegerProperty value;
	private ObjectProperty<Player> owner;
	private int oldValue;
	
	public Dice() {
		this.oldValue = 0;
		this.value = new SimpleIntegerProperty();
		this.owner = new SimpleObjectProperty<Player>();
	}
	
	public void throwDice() {
		Random rand = new Random();
		int newValue;
		do {
			newValue = rand.nextInt(DICE_MAX) + 1;
		} while (newValue == oldValue); // don't accept two identical throws in a row
		oldValue = newValue;
		value.set(newValue);
	}
	
	public IntegerProperty valueProperty() {
		return value;
	}
	
	public int getValue() {
		return value.get();
	}
	
	public void setOwner(Player player) {
		owner.set(player);
	}
	
	public ObjectProperty<Player> ownerProperty() {
		return owner;
	}
}
