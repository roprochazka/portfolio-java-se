package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javafx.animation.PauseTransition;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.ObservableMap;
import javafx.scene.shape.Shape;
import javafx.util.Duration;
import view.SettingsManager;

public abstract class Player {
	protected PawnType pawnType;
	protected List<Pawn> playerPawns;
	private int pawnsPerPlayer; 
	protected BooleanProperty isProcessingTurn;
	protected BooleanProperty won;
	protected int moveablePawns;

	public Player(PawnType pawnType, SettingsManager settings) {
		this.pawnType = pawnType;
		this.playerPawns = new ArrayList<>();
		this.isProcessingTurn = new SimpleBooleanProperty(false);
		this.won = new SimpleBooleanProperty(false);
		this.moveablePawns = 0;
		this.pawnsPerPlayer = settings.getPawnsPerPlayer();
	}

	public void setPawnsAtStart(SettingsManager settings) {
		for (int j = 0; j < pawnsPerPlayer; j++) {
			int startingIndex = -pawnType.getEntranceIndex() - j - 1;
			Pawn pawn = new Pawn(pawnType, startingIndex, settings);
			playerPawns.add(pawn);
		}
	}

	public void play(Map<Integer, Shape> drawnPawns, ObservableMap<Integer, Pawn> pawns, Dice dice, SettingsManager settings) {
		startTurn();
		dice.throwDice();
		choosePawnAndMove(drawnPawns, pawns, dice, settings);
		passTurnIfMoveImpossible(dice, settings);

	}

	protected void startTurn() {
		isProcessingTurn.set(true); // mark the player's turn as being processed
	}

	protected void finishTurn() {
		isProcessingTurn.set(false); // mark the turn of the player as finished (which triggers the change listener
										// and delegetes the move to the next player)
	}

	protected abstract void choosePawnAndMove(Map<Integer, Shape> drawnPawns, ObservableMap<Integer, Pawn> pawns,
			Dice dice, SettingsManager settings);

	protected void checkIfWon() {
		updateWonProperty();
	}

	// passes turn if it isn't possible to move (wait and then change the
	// isProcessingTurn property).
	// In situation where move is possible the property is changed in the choose and
	// move method.
	private void passTurnIfMoveImpossible(Dice dice, SettingsManager settings) {
		if (playerPawns.stream().noneMatch(x -> isMoveable(x, dice, settings))) { // the player can't move
			PauseTransition wait = new PauseTransition(Duration.seconds(1)); // wait before finishing turn
			wait.setOnFinished((e) -> {
				finishTurn();
			});
			wait.setCycleCount(1);
			wait.play();

		}
	}

	protected boolean isMoveable(Pawn pawn, Dice dice, SettingsManager settings) {
		if (dice.getValue() == 6 && hasPawnsAtStart() && pawn.isInPlay()) {
			// if the pawn isn't in starting position, there are any pawns in starting
			// positions and 6 has been thrown
			// i.e. if 6 has been thrown, the player has to put new pawn in the game
			return false;
		}

		if (hasPawnAtEntrance() && hasPawnsAtStart() && !pawn.isAtEntrance()) {
			// if there is a pawn at the entrance and there are pawns at the starting
			// positions, the pawn at the entrance has to be played.
			return false;
		}

		if (pawn.getPawnType() != pawnType) { // if the pawn isn't the same color as the player
			return false;
		}
		if (pawn.isAtStart() && dice.getValue() != 6) { // if the pawn is in the starting position and 6 was not
														// thrown
			return false;
		}
		Pawn testPawn = new Pawn(pawnType, pawn.getIndex(), settings); // create a clone to test the possibility to move
		testPawn.move(dice.getValue());
		for (Pawn otherPawn : playerPawns) {
			if (otherPawn.getIndex() == testPawn.getIndex() && !(pawn.isAtEntrance() && hasPawnsAtStart())) {
				return false; // can't move because the player would throw out himself - unless the pawn is
								// coming from the entrance and there are pawns at start
			}
		}
		if (testPawn.isOutOfBoard()) {
			return false; // can't move because the pawn would go out of the board
		}

		return true;
	}

	private boolean hasPawnAtEntrance() {
		return playerPawns.stream().anyMatch(x -> x.getIndex() == pawnType.getEntranceIndex());
	}

	private boolean hasPawnsAtStart() {
		return playerPawns.stream().anyMatch(x -> x.getIndex() < 0);
	}

	protected void updateWonProperty() {
		won.set(playerPawns.stream().map(x -> x.getIndex()).allMatch(x -> x >= 100)); // checking if all pawns are at
																						// ending positions
	}

	public void movePawn(Pawn pawn, int diceValue, ObservableMap<Integer, Pawn> pawns) {
		int index = pawn.getIndex();
		pawns.remove(index);// delete the "old" pawn
		if (index < 0) { // from the starting index (-1) only a jump to the entrance field is possible
			diceValue = 1;
		}
		pawn.move(diceValue); // change the index of the pawn
		int newIndex = pawn.getIndex();
		if (pawns.containsKey(newIndex)) {
			throwOut(pawns.get(newIndex), pawns);
		}
		pawns.put(newIndex, pawn); // add the "new" pawn
	}

	public void throwOut(Pawn pawn, ObservableMap<Integer, Pawn> pawns) {
		pawns.remove(pawn.getIndex());
		pawn.returnToStart();
		pawns.put(pawn.getIndex(), pawn);
	}

	public boolean hasAllPawnsAtStart() {
		for (Pawn pawn : playerPawns) {
			if (pawn.getIndex() >= 0) {
				return false;
			}
		}
		return true;
	}

	public List<Pawn> getPlayerPawns() {
		return playerPawns;
	}

	public PawnType getPawnType() {
		return pawnType;
	}

	public BooleanProperty isProcessingTurn() {
		return isProcessingTurn;
	}

	public BooleanProperty hasWon() {
		return won;
	}

}
