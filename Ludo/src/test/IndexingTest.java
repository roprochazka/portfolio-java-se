package test;

import model.Board;

public class IndexingTest {
	public static void main(String[] args) {
		Board board = new Board();
		for (int index = 0; index < 40; index++) {
			System.out.println("index:" + index + " column: " + board.getColumnFromIndex(index) + " row: "
					+ board.getRowFromIndex(index));
		}

	}

}
