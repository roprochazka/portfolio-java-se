package view;

import java.util.Optional;

import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Dialog;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class SettingsManager {
	private int numberOfEnemies;
	private int pawnsPerPlayer;
	private String colorOfHumanPlayer;

	private Integer[] enemiesNumbers = { 1, 2, 3 };
	private Integer[] pawnsNumbers = { 1, 2, 3, 4 };
	private String[] colors = { "Blue", "Red", "Green", "Black" };

	public SettingsManager() {
		numberOfEnemies = 3;
		pawnsPerPlayer = 4;
		colorOfHumanPlayer = "Blue";
	}

	private void askForNumberOfEnemies() {
		ChoiceDialog<Integer> enemiesDialog = new ChoiceDialog<Integer>(3, enemiesNumbers);
		enemiesDialog.setTitle("Number of enemies");
		enemiesDialog.setHeaderText("Choose the number of enemies:");
		undecorateDialog(enemiesDialog);
		Optional<Integer> result = enemiesDialog.showAndWait();
		if (result.isPresent()) {
			numberOfEnemies = result.get();
		}
	}

	private void askForPawnsPerPlayer() {
		ChoiceDialog<Integer> pawnsDialog = new ChoiceDialog<Integer>(4, pawnsNumbers);
		pawnsDialog.setTitle("Number of pawns");
		pawnsDialog.setHeaderText("Choose the number of pawns per player:");
		undecorateDialog(pawnsDialog);
		Optional<Integer> result = pawnsDialog.showAndWait();
		if (result.isPresent()) {
			pawnsPerPlayer = result.get();
		}
	}

	private void askForColor() {
		ChoiceDialog<String> colorDialog = new ChoiceDialog<String>("Blue", colors);
		colorDialog.setTitle("Color");
		colorDialog.setHeaderText("Choose your color:");
		undecorateDialog(colorDialog);
		Optional<String> result = colorDialog.showAndWait();
		if (result.isPresent()) {
			colorOfHumanPlayer = result.get();
		} 
		
	}

	public void askForSettings() {
		askForNumberOfEnemies();
		askForPawnsPerPlayer();
		askForColor();
	}
	
	private <R> void undecorateDialog(Dialog<R> dialog) {

		dialog.getDialogPane().getButtonTypes().remove(ButtonType.CANCEL);
		Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
		stage.initStyle(StageStyle.UNDECORATED);
		
	}

	public int getNumberOfEnemies() {
		return numberOfEnemies;
	}

	public Color getColorOfHumanPlayer() {
		switch(colorOfHumanPlayer){
		case "Blue": return Color.BLUE;
		case "Red": return Color.RED;
		case "Green": return Color.GREEN;
		case "Black" : return Color.BLACK;
		default: return Color.BLUE;
		}
	}

	public int getPawnsPerPlayer() {
		return pawnsPerPlayer;
	}

}
