package view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import model.Dice;

public class DicePainter {
	private FlowPane diceFrame;
	private Pane drawnDice;
	
	private final static double DOT_RADIUS = 5.0;
	private final static double DICE_SIDE = 50.0;
	
	// positions of dots on the dice:
	private final static double[] MIDDLE = {0.5, 0.5};
	private final static double[] UPPER_LEFT = {0.25, 0.25};
	private final static double[] LOWER_RIGHT = {0.75, 0.75};
	private final static double[] UPPER_RIGHT = {0.75, 0.25};
	private final static double[] LOWER_LEFT = {0.25, 0.75};
	private final static double[] LEFT = {0.25, 0.5};
	private final static double[] RIGHT = {0.75, 0.5};

	public DicePainter() {
		diceFrame = new FlowPane();
		drawnDice = new Pane();
		diceFrame.setAlignment(Pos.CENTER);
		diceFrame.setPadding(new Insets(20));
		
		drawnDice.setPrefSize(DICE_SIDE, DICE_SIDE);
		drawnDice.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(10), null)));
		drawnDice.setBackground(new Background(new BackgroundFill(Color.BISQUE, new CornerRadii(10), null)));
		diceFrame.getChildren().add(drawnDice);
	}
	
	public void showPlayerColor(Color playerColor) {
		diceFrame.setBackground(new Background(new BackgroundFill(playerColor, null, new Insets(5))));
	}
	
	public void drawDice(Dice dice) {
		
		drawnDice.getChildren().clear(); // delete old dice
		int diceValue = dice.getValue();
		 
		if (diceValue == 1 || diceValue == 3 || diceValue == 5) {
			drawnDice.getChildren().add(dot(MIDDLE));
		}
		if (diceValue == 2 || diceValue == 3 || diceValue == 4 || diceValue == 5 || diceValue == 6) {
			drawnDice.getChildren().addAll(dot(UPPER_RIGHT), dot(LOWER_LEFT));
		}
		if (diceValue == 4 || diceValue == 5 || diceValue == 6) {
			drawnDice.getChildren().addAll(dot(UPPER_LEFT), dot(LOWER_RIGHT));
		}
		if (diceValue == 6) {
			drawnDice.getChildren().addAll(dot(RIGHT), dot(LEFT));
		}		
	}
	
	private Circle dot(double[] position) {
		Circle circle = new Circle(DOT_RADIUS);
		circle.setCenterX(position[0] * DICE_SIDE); 
		circle.setCenterY(position[1] * DICE_SIDE);
		return circle;
	}
	
	public Pane getDiceFrame() {
		return diceFrame;
	}
}
