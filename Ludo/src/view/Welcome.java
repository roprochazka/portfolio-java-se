package view;

import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Welcome {
	private final static String rules = " The objective of the player is to move all pawns from the starting positions (colored fields in the corners of the board) to the ending positions (colored fields in the center of the board, marked by an arrow) following the path around the board in clockwise direction.\r\n"
			+ "\r\n" + " The following rules apply:\r\n" + "\r\n"
			+ "a)  The BLUE player is the first to throw a dice and the following players take turns in clockwise direction.\r\n"
			+ "b)  If a player throws a 6 and there are pawns at his starting positions, he HAS TO move a pawn from the starting position to the entrance position (the colored field in the path near the starting positions). The game only allows such movements in that case.\r\n"
			+ "c)  Any time a player throws a 6, he has the right to do another turn (after finishing his first turn he throws the dice again). The game takes care of this automatically.\r\n"
			+ "d)  A player's pawn which has been moved to the player's entrance position has to be moved further by the next move, unless there are no pawns at the player's starting positions. This game obliges the player to do so automatically.\r\n"
			+ "e)  A pawn throws out an enemy pawn which is standing at the target field of his move. The enemy pawn moves back to it's starting position.\r\n"
			+ "f)  A pawn can't throw out a pawn of the same color (the attacking pawn can't be moved) unless it's obliged to do so in order to make the entrance position free. \r\n"
			+ "g)  In order to win the game, the player has to align all his pawns at the ending positions. \r\n"
			+ "h)  A pawn standing in front of the ending positions has to make a valid move to fit on the fields among the pawns already standing at the ending positions. If the dice value doesn't allow it (the number is too large or too small), the pawn can't be moved.\r\n"
			+ "i)  Pawns at the ending positions can be moved forward as long as the dice value allows a movement to another free ending position.\r\n"
			+ "j)  If a player can't move any of his pawns, he passes the turn to the next player (or throws again, if 6 has been thrown). This happens automatically in this computer game.\r\n"
			+ "\r\n"
			+ " The game indicates for the human player the pawns which can be moved by rotating them to the right.\r\n"
			+ " The dice is being thrown automatically and a color surrounding the dice indicates the player who should move.\r\n"
			+ "\r\n" + " Good luck!";

	public static void showRules() {

		Dialog<ButtonType> dialog = new Dialog<>();
		dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
		dialog.setHeaderText("Welcome to the Ludo game!");
		dialog.setContentText(rules);
		Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
		stage.initStyle(StageStyle.UNDECORATED);
		dialog.getDialogPane().setMinWidth(650);
		dialog.showAndWait();
	}
}
