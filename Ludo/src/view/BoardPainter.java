package view;

import java.util.HashMap;
import java.util.Map;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import model.Board;
import model.Pawn;
import model.PawnType;

public class BoardPainter {

	public final static double FIELD_RADIUS = 25;
	public final static Color DEFAULT_FIELD_COLOR = Color.BEIGE;

	/*
	 * a map to collect all drawn pawns in order to access them by index and to move
	 * them
	 */
	private Map<Integer, Shape> drawnPawns;
	private Board board;
	private GridPane mainBoardFrame;
	
	public BoardPainter() {
		drawnPawns = new HashMap<>();
		this.board = new Board();
		mainBoardFrame = new GridPane();
		new AnchorPane();
	}

	public void initializeBoard(SettingsManager settings) {

		mainBoardFrame.setAlignment(Pos.CENTER);
		mainBoardFrame.setPadding(new Insets(5));
		mainBoardFrame.setHgap(0);
		mainBoardFrame.setVgap(0);

		drawStartingFields(settings);
		drawPlayingFields();
		drawEndingFields(settings);
		drawEntranceFields();
		drawEndingArrows();
	}

	private void drawStartingFields(SettingsManager settings) {
		for (PawnType squadType : PawnType.values()) {
			int entranceIndex = squadType.getEntranceIndex();
			for (int i = 0; i < settings.getPawnsPerPlayer(); i++) {
				Circle circle = drawField(squadType.getColor());
				int startingFieldIndex = -entranceIndex - i - 1;
				addAtPosition(circle, startingFieldIndex, mainBoardFrame);
			}

		}

	}

	private void drawEndingFields(SettingsManager settings) {
		for (PawnType squadType : PawnType.values()) {
			int entranceIndex = squadType.getEntranceIndex();
			for (int i = 0; i < settings.getPawnsPerPlayer(); i++) {
				Circle circle = drawField(squadType.getColor());
				int endingFieldIndex = 100 + entranceIndex + i;
				addAtPosition(circle, endingFieldIndex, mainBoardFrame);
			}
		}
	}

	private void drawPlayingFields() {
		for (int i = 0; i < Board.PLAYING_FIELDS_NUMBER; i++) {
			Circle circle = drawField();
			addAtPosition(circle, i, mainBoardFrame);
		}
	}

	private void drawEntranceFields() {
		for (PawnType squadType : PawnType.values()) {
			int index = squadType.getEntranceIndex();
			Circle drawnField = drawField(squadType.getColor());
			addAtPosition(drawnField, index, mainBoardFrame);
		}
	}

	public void drawPawn(Pawn pawn) {
		int index = pawn.getIndex();
		double headRadius = 8;
		Circle head = new Circle();
		head.setRadius(headRadius);
		Polygon body = new Polygon(0.0, 0.0, -12.0, 25.0, 12.0, 25.0);
		Shape drawnPawn = Shape.union(head, body);

		Stop[] stops = { new Stop(0, Color.WHITE), new Stop(1, pawn.getColor()) };
		RadialGradient gradient = new RadialGradient(-60, 0.5, 0.5, 0.38, 0.55, true, CycleMethod.NO_CYCLE, stops);
		drawnPawn.setFill(gradient);
		drawnPawn.setStroke(Color.WHITE);
		addAtPosition(drawnPawn, index, mainBoardFrame);
		drawnPawns.put(index, drawnPawn);
		GridPane.setHalignment(drawnPawn, HPos.CENTER);
		GridPane.setValignment(drawnPawn, VPos.CENTER);

	}

	public Shape removePawn(Pawn pawn) {
		int index = pawn.getIndex();
		Shape drawnPawn = drawnPawns.get(index);
		mainBoardFrame.getChildren().remove(drawnPawn);
		return drawnPawn;
	}

	public void addIndexes() {
		for (int i = 0; i < Board.PLAYING_FIELDS_NUMBER; i++) {
			Text text = new Text(String.valueOf(i));
			GridPane.setHalignment(text, HPos.CENTER);
			GridPane.setValignment(text, VPos.CENTER);
			addAtPosition(text, i, mainBoardFrame);
		}
	}

	private void addAtPosition(Node node, int i, GridPane mainBoardFrame) {
		mainBoardFrame.add(node, board.getColumnFromIndex(i), board.getRowFromIndex(i));
	}

	private Circle drawField() {
		Color fieldBorderColor = Color.BROWN;
		Circle drawnField = drawField(DEFAULT_FIELD_COLOR);
		drawnField.setStroke(fieldBorderColor);
		return drawnField;
	}

	private Circle drawField(Color color) {
		Color fieldBorderColor = Color.BLACK;
		Circle drawnField = new Circle(FIELD_RADIUS);
		drawnField.setFill(color);
		drawnField.setStroke(fieldBorderColor);
		return drawnField;
	}

	private Polygon drawArrow(Color color, double rotationAngle) {
		Polygon arrow = new Polygon();
		arrow.getPoints().addAll(
				new Double[] { 0.0, 0.0, -20.0, -15.0, -18.0, -6.0, -40.0, -6.0, -40.0, 6.0, -18.0, 6.0, -20.0, 15.0 });
		arrow.setFill(color);
		GridPane.setHalignment(arrow, HPos.CENTER);
		GridPane.setValignment(arrow, VPos.CENTER);
		arrow.setRotate(rotationAngle);
		return arrow;
	}

	private void drawEndingArrows() {
		for (int i = 0; i < PawnType.values().length; i++) {
			PawnType squadType = PawnType.values()[i];
			int exitIndex = squadType.getExitIndex();
			Polygon arrow = drawArrow(squadType.getColor(), 90 * i);
			addAtPosition(arrow, exitIndex, mainBoardFrame);
		}
	}

	public Map<Integer, Shape> getDrawnPawns() {
		return drawnPawns;
	}
	
	public GridPane getMainBoardFrame() {
		return mainBoardFrame;
	}
}
