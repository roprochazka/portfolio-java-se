package controller;

import java.util.List;
import java.util.Map;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener.Change;
import javafx.collections.ObservableMap;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.stage.Stage;
import model.AIPlayer;
import model.Dice;
import model.HumanPlayer;
import model.Pawn;
import model.PawnType;
import model.Player;
import view.BoardPainter;
import view.DicePainter;
import view.SettingsManager;

public class GameController {
	private ObservableMap<Integer, Pawn> pawns;
	private Map<Integer, Shape> drawnPawns;
	private int playersNumber;
	private Player[] players;
	private Dice dice;
	private boolean gameActive;

	public GameController(BoardPainter boardPainter, DicePainter dicePainter, SettingsManager settings) {
		gameActive = true;
		dice = new Dice();
		pawns = FXCollections.observableHashMap();
		drawnPawns = boardPainter.getDrawnPawns();
		playersNumber = settings.getNumberOfEnemies() + 1;
		players = new Player[playersNumber];

		// set change listeners to maps of pawns
		pawns.addListener((Change<? extends Integer, ? extends Pawn> c) -> {
			if (c.wasAdded()) {
				Pawn addedPawn = c.getValueAdded();
				boardPainter.drawPawn(addedPawn);
			} else if (c.wasRemoved()) {
				Pawn removedPawn = c.getValueRemoved();
				boardPainter.removePawn(removedPawn);
			}
		});

		// set change listener to the owner of the dice in order to change the color of
		// the dice
		dice.ownerProperty().addListener(new ChangeListener<Player>() {

			@Override
			public void changed(ObservableValue<? extends Player> observable, Player oldOwner, Player newOwner) {
				dicePainter.showPlayerColor(newOwner.getPawnType().getColor());

			}

		});

		// set change listener to dice value
		dice.valueProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				dicePainter.drawDice(dice);
			}
		});
	}

	// returns the array of pawn types needed in the play according to the settings.
	// Starts at BLUE and follows the order of pawn types
	// as defined in the enum. The BLUE and the color of the human player is always
	// added. The remaining colors only if more enemies are to be crated.
	private PawnType[] getTypesOrder(SettingsManager settings) {
		int numberOfEnemies = settings.getNumberOfEnemies();
		PawnType[] types = new PawnType[playersNumber];
		Color colorOfHumanPlayer = settings.getColorOfHumanPlayer();
		int numberOfEnemiesChosen = 0;
		int position = 0;
		for (int i = 0; i < 4; i++) {
			if (PawnType.values()[i].getColor() != colorOfHumanPlayer) {
				if (numberOfEnemiesChosen < numberOfEnemies) { // not enough types of enemies added to the array
					types[position] = PawnType.values()[i];
					numberOfEnemiesChosen++;
					position++;
				}
			} else {
				types[position] = PawnType.values()[i]; // add the human player to the array
				position++;
			}
		}
		return types;
	}

	public void initializePlayers(SettingsManager settings) {
		PawnType[] typeOrder = getTypesOrder(settings);
		Color colorOfHumanPlayer = settings.getColorOfHumanPlayer();
		for (int i = 0; i < playersNumber; i++) {
			PawnType pawnType = typeOrder[i];
			Player player;
			if (pawnType.getColor() == colorOfHumanPlayer) {
				player = new HumanPlayer(pawnType, settings);
			} else {
				player = new AIPlayer(pawnType, settings);
			}
			players[i] = player;
			player.setPawnsAtStart(settings);
			List<Pawn> playerPawns = player.getPlayerPawns();
			playerPawns.forEach(pawn -> pawns.put(pawn.getIndex(), pawn)); // put player pawns into the general pawns
																			// observable map
			setWinningListener(player);
		}
	}

	public void startGame(SettingsManager settings) {
		dice.throwDice();
		setPlayingTurns(settings);
		dice.setOwner(players[0]);
		players[0].play(drawnPawns, pawns, dice, settings);
	}

	// sets the natural playing order of the players and registers the change
	// listeners to delegate the turn
	private void setPlayingTurns(SettingsManager settings) {
		for (int playerIndex = 0; playerIndex < playersNumber; playerIndex++) {
			Player thisPlayer = players[playerIndex];
			Player nextPlayer = players[(playerIndex + 1) % playersNumber];

			thisPlayer.isProcessingTurn().addListener(new ChangeListener<Boolean>() {
				@Override
				public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
					if (!newValue && gameActive) { // if the new value changed to false, i.e. player finished the turn
						if (dice.getValue() == 6) { // if 6 was thrown, the player plays again
							thisPlayer.play(drawnPawns, pawns, dice, settings);
						} else {
							dice.setOwner(nextPlayer);
							nextPlayer.play(drawnPawns, pawns, dice, settings);
						}
					}
				}
			});
		}
	}

	private void setWinningListener(Player player) {

		player.hasWon().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (newValue) { // if the new value changed to true, i.e. player won
					gameActive = false;
					Alert alert = new Alert(null, "The " + player.getPawnType() + " player won! Congratulations!",
							ButtonType.OK);
					alert.show();
					Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
					stage.getIcons().add(new Image("icon.jpg"));
					alert.setOnHidden(e -> Platform.exit());
				}
			}

		});
	}

}
