package report;

import java.util.Optional;

import controller.ObservableListController;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.Chart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import model.Language;
import model.Status;
import utils.DialogBuilder;

public class Reporter {

	public static void showReport(ObservableListController olc) {
		Optional<ReportType> optionalReportType = DialogBuilder.getReportTypeFromDialog();
		if (optionalReportType.isPresent()) {
			ReportType reportType = optionalReportType.get();
			Pane pane = new Pane();
			addChart(olc, reportType, pane);
			
			Scene scene = new Scene(pane);
			Stage stage = new Stage();
			stage.setScene(scene);

			stage.setTitle("Report");
			stage.getIcons().add(new Image("open-book.png"));
			stage.show();
		}
	}

	private static void addChart(ObservableListController olc, ReportType reportType, Pane pane) {
		switch (reportType) {
		case BAR_ALL_PAGES_PER_LANGUAGE:
			pane.getChildren().add(getBarChartAllPagesPerLanguage(olc));
			break;
		case PIE_PAGES_READ:
			pane.getChildren().add(getPieChartPagesRead(olc));
			break;
		}
	}

	private static Chart getPieChartPagesRead(ObservableListController olc) {
		ObservableList<PieChart.Data> data = FXCollections.observableArrayList();

		ObservableList<Language> languages = olc.getLanguages();
		for (Language lang : languages) {
			int langCountPages = olc.getBooks().stream()
					.filter(x -> x.getLanguage().equals(lang) && x.getStatus().equals(Status.READ))
					.mapToInt(x -> x.getPageCount()).sum();
			data.add(new PieChart.Data(lang.getLanguageName(), langCountPages));
		}
		PieChart pieChart = new PieChart(data);
		pieChart.setTitle("Page count read per language");

		pieChart.setLegendSide(Side.BOTTOM);

		data.forEach(d -> {
			int pieValueInteger = (int) d.pieValueProperty().get();
			d.nameProperty().bind(Bindings.concat(d.getName(), " (", pieValueInteger, " pages)"));
		});

		return pieChart;

	}

	private static Chart getBarChartAllPagesPerLanguage(ObservableListController olc) {
		XYChart.Series<String, Number> toRead = new XYChart.Series<>();
		XYChart.Series<String, Number> beingRead = new XYChart.Series<>();
		XYChart.Series<String, Number> read = new XYChart.Series<>();

		toRead.setName("To Read");
		beingRead.setName("Being Read");
		read.setName("Read");

		ObservableList<Language> languages = olc.getLanguages();
		for (Language lang : languages) {
			int readPages = olc.getBooks().stream()
					.filter(x -> x.getLanguage().equals(lang) && x.getStatus().equals(Status.READ))
					.mapToInt(x -> x.getPageCount()).sum();
			read.getData().add(new XYChart.Data<>(lang.getLanguageName(), readPages));

			int beingReadPages = olc.getBooks().stream()
					.filter(x -> x.getLanguage().equals(lang) && x.getStatus().equals(Status.BEING_READ))
					.mapToInt(x -> x.getPageCount()).sum();
			beingRead.getData().add(new XYChart.Data<>(lang.getLanguageName(), beingReadPages));

			int toReadPages = olc.getBooks().stream()
					.filter(x -> x.getLanguage().equals(lang) && x.getStatus().equals(Status.TO_READ))
					.mapToInt(x -> x.getPageCount()).sum();
			toRead.getData().add(new XYChart.Data<>(lang.getLanguageName(), toReadPages));
		}

		ObservableList<XYChart.Series<String, Number>> data = FXCollections.observableArrayList();
		data.add(read);
		data.add(beingRead);
		data.add(toRead);

		CategoryAxis xAxis = new CategoryAxis();
		xAxis.setLabel("Languages");
		NumberAxis yAxis = new NumberAxis();
		yAxis.setLabel("Pages");
		StackedBarChart<String, Number> chart = new StackedBarChart<>(xAxis, yAxis, data);
		chart.setTitle("Pages per language");

		return chart;
	}
}
