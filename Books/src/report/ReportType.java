package report;

public enum ReportType {
	PIE_PAGES_READ ("Pages read per language (Pie chart)"),
	BAR_ALL_PAGES_PER_LANGUAGE ("Pages per language per status (Stacked bar chart)");
	
	private String fullReportName;
	
	private ReportType(String fullReportName) {
		this.fullReportName = fullReportName;
	}
	
	public String getFullReportName() {
		return fullReportName;
	}
	
	public static ReportType fromString(String fullReportName) throws IllegalArgumentException {
		for (ReportType reportType : values()) {
			if (reportType.getFullReportName().equals(fullReportName)) {
				return reportType;
			}
		}
		throw new IllegalArgumentException();
	}

}
