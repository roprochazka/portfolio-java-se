package model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Author {
	private StringProperty authorName;
	private StringProperty authorSurname;
		
	public Author(String authorName, String authorSurname) {
		this.authorName = new SimpleStringProperty(authorName);
		this.authorSurname = new SimpleStringProperty(authorSurname);
	}
	
	public String getAuthorName() {
		return authorName.get();
	}
	
	public void setAuthorName(String authorName) {
		this.authorName.set(authorName);
	}
	
	public String getAuthorSurname() {
		return authorSurname.get();
	}
	
	public void setAuthorSurname(String authorSurname) {
		this.authorSurname.set(authorSurname);
	}

	public StringProperty authorNameProperty() {
		return authorName;
	}

	public StringProperty authorSurnameProperty() {
		return authorSurname;
	}
	
	@Override
	public String toString() {
		return "Author [authorName=" + authorName.get() + ", authorSurname=" + authorSurname.get() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((authorName == null) ? 0 : authorName.get().hashCode());
		result = prime * result + ((authorSurname == null) ? 0 : authorSurname.get().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Author other = (Author) obj;
		if (authorName.get() == null) {
			if (other.authorName.get() != null) {
				return false;
			}
		} else if (!authorName.get().equals(other.authorName.get())) {
			return false;
		}
		if (authorSurname.get() == null) {
			if (other.authorSurname.get() != null) {
				return false;
			}
		} else if (!authorSurname.get().equals(other.authorSurname.get())) {
			return false;
		}
		return true;
	}

	
}
