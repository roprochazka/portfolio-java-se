package model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Language {

	StringProperty languageName;

	public Language(String languageName) {
		this.languageName = new SimpleStringProperty(languageName);
	}

	public String getLanguageName() {
		return languageName.get();
	}

	public void setLanguageName(String languageName) {
		this.languageName.set(languageName);
	}

	public StringProperty languageNameProperty() {
		return languageName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((languageName == null) ? 0 : languageName.get().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Language other = (Language) obj;
		if (languageName.get() == null) {
			if (other.languageName.get() != null) {
				return false;
			}
		} else if (!languageName.get().equals(other.languageName.get())) {
			return false;
		}
		return true;
	}

}
