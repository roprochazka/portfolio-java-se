package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Book {
	private StringProperty title;
	private ObjectProperty<Author> author;
	private ObjectProperty<Language> language;
	private IntegerProperty pageCount;
	private ObjectProperty<Status> status;
	
	public Book(String title, Author author, Language language, int pageCount, Status status) {
		this.title = new SimpleStringProperty(title);
		this.author = new SimpleObjectProperty<>(author);
		this.language = new SimpleObjectProperty<>(language);
		this.pageCount = new SimpleIntegerProperty(pageCount);
		this.status = new SimpleObjectProperty<>(status);
	}
	public String getTitle() {
		return title.get();
	}
	
	public void setTitle(String title) {
		this.title.set(title);
	}
	
	public Author getAuthor() {
		return author.get();
	}
	
	public void setAuthor(Author author) {
		this.author.set(author);
	}
	
	public Language getLanguage() {
		return language.get();
	}
	
	public void setLanguage(Language language) {
		this.language.set(language);
	}
	
	public int getPageCount() {
		return pageCount.get();
	}
	
	public void setPageCount(int pageCount) {
		this.pageCount.set(pageCount);
	}
	
	public Status getStatus() {
		return status.get();
	}
	
	public void setStatus(Status status) {
		this.status.set(status);
	}

	public StringProperty titleProperty() {
		return title;
	}

	public IntegerProperty pageCountProperty() {
		return pageCount;
	}
	
	public ObjectProperty<Author> authorProperty(){
		return author;
	}
	
	public ObjectProperty<Language> languageProperty(){
		return language;
	}
	
	public ObjectProperty<Status> statusProperty(){
		return status;
	}
	
	public StringProperty authorNameProperty() {
		return author.get().authorNameProperty();
	}
	
	public StringProperty authorSurnameProperty() {
		return author.get().authorSurnameProperty();
	}
		
	public StringProperty statusNameProperty() {
		return status.get().statusNameProperty();
	}
	
	public StringProperty languageNameProperty() {
		return language.get().languageNameProperty();
	}
	
	public Book copy() {
		return new Book(title.get(), author.get(), language.get(), pageCount.get(), status.get()); 
	}

	@Override
	public String toString() {
		return "Book [title=" + title.get() + ", author=" + author.get() + ", language=" + language.get()
				+ ", pageCount=" + pageCount.get() + ", status=" + status.get() + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.get().hashCode());
		result = prime * result + ((language == null) ? 0 : language.get().hashCode());
		result = prime * result + ((title == null) ? 0 : title.get().hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (author.get() == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (language == null) {
			if (other.language != null)
				return false;
		} else if (!language.equals(other.language))
			return false;
		if (title.get() == null) {
			if (other.title.get() != null)
				return false;
		} else if (!title.get().equals(other.title.get()))
			return false;
		return true;
	}
	public void setPropertiesFromAnotherBook(Book newBook) {
		setTitle(newBook.titleProperty().get());
		setAuthor(newBook.authorProperty().get());
		setLanguage(newBook.languageProperty().get());
		setPageCount(newBook.pageCountProperty().get());
		setStatus(newBook.statusProperty().get());
	}

}
