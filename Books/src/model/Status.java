package model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public enum Status {
	READ("read"), BEING_READ("being read"), TO_READ("to read");

	private StringProperty statusName;

	private Status(String statusName) {
		this.statusName = new SimpleStringProperty(statusName);
	}

	public String getStatusName() {
		return statusName.get();
	}

	public StringProperty statusNameProperty() {
		return statusName;
	}

	public static ObservableList<String> observableStatusNames() {
		ObservableList<String> observableStatusNames = FXCollections.observableArrayList();
		for (Status status : values()) {
			observableStatusNames.add(status.getStatusName());
		}
		return observableStatusNames;
	}

	public static Status fromString(String statusName) throws IllegalArgumentException {
		for (Status status : values()) {
			if (status.getStatusName().equals(statusName)) {
				return status;
			}
		}
		throw new IllegalArgumentException();
	}
}
