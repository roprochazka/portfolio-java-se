package exceptions;

import utils.DialogBuilder;

public class DBException extends Exception {
	
	private static final long serialVersionUID = 1087737210439121223L;

	public DBException(String message) {
		DialogBuilder.getAlert("Unexpected exception", "A database exception occurred!", message);
	}

}
