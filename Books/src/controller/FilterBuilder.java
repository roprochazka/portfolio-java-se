package controller;

import java.util.function.Predicate;

import model.Book;

public class FilterBuilder {
	private String placeHolder;

	public FilterBuilder(String placeHolder) {
		this.placeHolder = placeHolder;
	}

	private Predicate<Book> titleFilter(String title) {
		return x -> "".equals(title) || title == null ? true : x.getTitle().toUpperCase().contains(title.toUpperCase());
	}

	private Predicate<Book> authorNameFilter(String authorName) {
		return x -> "".equals(authorName) || authorName == null ? true
				: x.getAuthor().getAuthorName().toUpperCase().contains(authorName.toUpperCase());
	}

	private Predicate<Book> authorSurnameFilter(String authorSurname) {
		return x -> "".equals(authorSurname) || authorSurname == null ? true
				: x.getAuthor().getAuthorSurname().toUpperCase().contains(authorSurname.toUpperCase());
	}

	private Predicate<Book> languageFilter(String languageName) {
		return x -> placeHolder.equals(languageName) ? true
				: x.getLanguage().getLanguageName().toUpperCase().equals(languageName.toUpperCase());
	}

	private Predicate<Book> statusFilter(String statusName) {
		return x -> placeHolder.equals(statusName) ? true
				: x.getStatus().getStatusName().toUpperCase().equals(statusName.toUpperCase());
	}

	/**
	 * 
	 * @param bookFilter
	 * @param placeHolder
	 *            the String value used when the user doesn't want to choose any
	 *            value in a combo box
	 * @return a predicate representing the filter (the intersection of all
	 *         conditions)
	 */
	public Predicate<Book> getFilter(BookFilter bookFilter) {
		String title = bookFilter.getTitle();
		String authorName = bookFilter.getAuthorName();
		String authorSurname = bookFilter.getAuthorSurname();
		String language = bookFilter.getLanguage();
		String status = bookFilter.getStatus();

		return titleFilter(title).and(authorNameFilter(authorName)).and(authorSurnameFilter(authorSurname))
				.and(languageFilter(language)).and(statusFilter(status));
	}

}
