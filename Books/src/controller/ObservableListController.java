package controller;

import java.util.List;
import java.util.stream.Collectors;

import database.AuthorDAO;
import database.BookDAO;
import database.LanguageDAO;
import exceptions.DBException;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener.Change;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import model.Author;
import model.Book;
import model.Language;

public class ObservableListController {
	private ObservableList<Book> booksNotFiltered;
	private FilteredList<Book> books;
	private ObservableList<Author> authors;
	private ObservableList<Language> languages;
	private ObservableList<String> languageNames;
	private ObservableList<String> languageNamesWithPlaceHolder;

	BookDAO bookDAO;
	AuthorDAO authorDAO;
	LanguageDAO languageDAO;

	public ObservableListController(String placeHolder) {
		
		booksNotFiltered = FXCollections.observableArrayList();
		authors = FXCollections.observableArrayList();
		languages = FXCollections.observableArrayList();
		languageNames = FXCollections.observableArrayList();
		languageNamesWithPlaceHolder = FXCollections.observableArrayList();
		
		languageNamesWithPlaceHolder.add(placeHolder);

		bookDAO = new BookDAO();
		authorDAO = new AuthorDAO();
		languageDAO = new LanguageDAO();

		listenToListChanges();
		
	}

	private void listenToListChanges() {
		booksNotFiltered.addListener((Change<? extends Book> change) -> {
			while (change.next()) {
				if (change.wasAdded()) {
					for (Book addedBook : change.getAddedSubList()) {
						try {
							deleteUnusedAuthors();
							Author addedBookAuthor = addedBook.getAuthor();
							if (!authors.contains(addedBookAuthor)) {
								authors.add(addedBookAuthor);
							}
							bookDAO.create(addedBook);
							listenToPropertyChanges(addedBook); // after adding a book we register new property change
																// listeners to the book
						} catch (DBException e) {
							e.printStackTrace();
						}
					}
				}

				if (change.wasRemoved()) {
					for (Book removedBook : change.getRemoved()) {
						try {
							bookDAO.delete(removedBook);
						} catch (DBException e) {
							e.printStackTrace();
						}
					}
				}
			}
		});

		authors.addListener((Change<? extends Author> change) -> {
			while (change.next()) {
				if (change.wasAdded()) {
					for (Author addedAuthor : change.getAddedSubList()) {
						try {
							authorDAO.create(addedAuthor);
						} catch (DBException e) {
							e.printStackTrace();
						}
					}
				}

				if (change.wasRemoved()) {
					for (Author removedAuthor : change.getRemoved()) {
						try {
							authorDAO.delete(removedAuthor);
						} catch (DBException e) {
							e.printStackTrace();
						}
					}
				}
			}
		});
		languages.addListener((Change<? extends Language> change) -> {
			while (change.next()) {
				if (change.wasAdded()) {
					for (Language addedLanguage : change.getAddedSubList()) {
						try {
							languageDAO.create(addedLanguage);
							languageNames.add(addedLanguage.getLanguageName());
							languageNamesWithPlaceHolder.add(addedLanguage.getLanguageName());
						} catch (DBException e) {
							e.printStackTrace();
						}
					}
				}

				if (change.wasRemoved()) {
					for (Language removedLanguage : change.getRemoved()) {
						try {
							languageDAO.delete(removedLanguage);
						} catch (DBException e) {
							e.printStackTrace();
						}
					}
				}
			}
		});
	}

	private void listenToPropertyChanges(Book addedBook) {
		addedBook.titleProperty().addListener((observable, oldValue, newValue) -> {
			try {
				System.out.println("changing title");
				Book oldBook = addedBook.copy();
				oldBook.setTitle(oldValue); // we need a copy of the book with the property yet unchanged
				bookDAO.update(oldBook, addedBook);
			} catch (DBException e) {
				// the change wasn't saved
			}
		});

		addedBook.authorProperty().addListener((observable, oldValue, newValue) -> {
			try {
				System.out.println("changing author");
				if (!authors.contains(newValue)) {
					authors.add(newValue);
				}
				Book oldBook = addedBook.copy();
				oldBook.setAuthor(oldValue);
				bookDAO.update(oldBook, addedBook);

			} catch (DBException e) {
				// the change wasn't saved
			}
		});

		addedBook.languageProperty().addListener((observable, oldValue, newValue) -> {
			try {
				Book oldBook = addedBook.copy();
				oldBook.setLanguage(oldValue);
				bookDAO.update(oldBook, addedBook);
			} catch (DBException e) {
				// the change wasn't saved
			}
		});

		addedBook.pageCountProperty().addListener((observable, oldValue, newValue) -> {
			try {
				Book oldBook = addedBook.copy();
				oldBook.setPageCount(oldValue.intValue());
				bookDAO.update(oldBook, addedBook);
			} catch (DBException e) {
				// the change wasn't saved
			}
		});

		addedBook.statusProperty().addListener((observable, oldValue, newValue) -> {
			try {
				Book oldBook = addedBook.copy();
				oldBook.setStatus(oldValue);
				bookDAO.update(oldBook, addedBook);
			} catch (DBException e) {
				// the change wasn't saved
			}
		});
	}

	public void filter(BookFilter bookFilter, String placeHolder) {
		FilterBuilder filterCreator = new FilterBuilder(placeHolder);
		books.setPredicate(filterCreator.getFilter(bookFilter));
	}

	public void fetchFromDB() throws DBException {

		BookDAO bookDAO = new BookDAO();
		AuthorDAO authorDAO = new AuthorDAO();
		LanguageDAO languageDAO = new LanguageDAO();

		List<Book> booksFromDB = bookDAO.readAll();
		List<Author> authorsFromDB = authorDAO.readAll();
		List<Language> languagesFromDB = languageDAO.readAll();

		booksNotFiltered.clear();
		authors.clear();
		languages.clear();

		booksNotFiltered.addAll(booksFromDB);

		// enabling the books observable list to be filtered (by making it an
		// FilteredList)
		// default filter x -> true doesn't have any effect:
		books = booksNotFiltered.filtered(x -> true);

		authors.addAll(authorsFromDB);
		languages.addAll(languagesFromDB);
	}

	private void deleteUnusedAuthors() {
		List<Author> usedAuthors = booksNotFiltered.stream().map(book -> book.getAuthor()).collect(Collectors.toList());
		authors.removeIf(author -> !usedAuthors.contains(author));
	}

	public void add(Book book) {
		booksNotFiltered.add(book);
	}

	public void add(Language language) {
		if (!languages.contains(language)) {
			languages.add(language);
		}
	}

	public void delete(Book book) {
		booksNotFiltered.remove(book);

	}

	public ObservableList<Book> getBooks() {
		return books;
	}

	public ObservableList<Author> getAuthors() {
		return authors;
	}

	public ObservableList<Language> getLanguages() {
		return languages;
	}
	
	public ObservableList<String> getLanguageNames() {
		return languageNames; 
	}
	
	public ObservableList<String> getLanguageNamesWithPlaceHolder(){
		return languageNamesWithPlaceHolder;
	}

}
