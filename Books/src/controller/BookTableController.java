package controller;

import java.util.Optional;

import javafx.collections.transformation.SortedList;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.IntegerStringConverter;
import model.Author;
import model.Book;
import model.Language;
import model.Status;
import utils.DialogBuilder;

public class BookTableController {
	private TableView<Book> bookTV;
	private SortedList<Book> sortedData;
	private ObservableListController olc;

	public BookTableController(TableView<Book> bookTV, MainController mpc, ObservableListController olc) {
		this.bookTV = bookTV;
		this.olc = olc;

		// enabling the filtered list to be sorted by wrapping it by a sorted list, bind
		// the comparator to the table view comparator (clicking the headers of the
		// table view will sort the list):

		this.sortedData = olc.getBooks().sorted();
		sortedData.comparatorProperty().bind(bookTV.comparatorProperty());

		fetchData();
	}

	public void createColumns() {
		bookTV.getColumns().clear();
		TableColumn<Book, String> titleColumn = new TableColumn<>("Title");
		TableColumn<Book, String> authorNameColumn = new TableColumn<>("Author Name");
		TableColumn<Book, String> authorSurnameColumn = new TableColumn<>("Author Surname");
		TableColumn<Book, String> languagesColumn = new TableColumn<>("Language");
		TableColumn<Book, Integer> pagesCountColumn = new TableColumn<>("Pages");
		TableColumn<Book, String> statusColumn = new TableColumn<>("Status");

		bookTV.getColumns().add(titleColumn);
		bookTV.getColumns().add(authorNameColumn);
		bookTV.getColumns().add(authorSurnameColumn);
		bookTV.getColumns().add(languagesColumn);
		bookTV.getColumns().add(pagesCountColumn);
		bookTV.getColumns().add(statusColumn);

		titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
		authorNameColumn.setCellValueFactory(new PropertyValueFactory<>("authorName"));
		authorSurnameColumn.setCellValueFactory(new PropertyValueFactory<>("authorSurname"));
		languagesColumn.setCellValueFactory(new PropertyValueFactory<>("languageName"));
		pagesCountColumn.setCellValueFactory(new PropertyValueFactory<>("pageCount"));
		statusColumn.setCellValueFactory(new PropertyValueFactory<>("statusName"));

		bookTV.setEditable(true);
		bookTV.getColumns().forEach(x -> x.setEditable(true));

		titleColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		titleColumn.setOnEditCommit(x -> {
			Book selectedBook = bookTV.getSelectionModel().getSelectedItem();
			selectedBook.setTitle(x.getNewValue());
		});

		authorNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		authorNameColumn.setOnEditCommit(authorName -> {
			Book selectedBook = bookTV.getSelectionModel().getSelectedItem();
			Author author = new Author(authorName.getNewValue(), selectedBook.getAuthor().getAuthorSurname());
			if (!olc.getAuthors().contains(author)) {
				olc.getAuthors().add(author);
			}
			selectedBook.setAuthor(author);
		});

		authorSurnameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		authorSurnameColumn.setOnEditCommit(authorSurname -> {
			Book selectedBook = bookTV.getSelectionModel().getSelectedItem();
			Author author = new Author(selectedBook.getAuthor().getAuthorName(), authorSurname.getNewValue());
			if (!olc.getAuthors().contains(author)) {
				olc.getAuthors().add(author);
			}
			selectedBook.setAuthor(author);
		});

		pagesCountColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter() {
			@Override
			public String toString(Integer x) {
				return String.valueOf(x);
			}

			@Override
			public Integer fromString(String s) {
				try {
					return Integer.parseInt(s);
				} catch (NumberFormatException e) {
					DialogBuilder.getAlert("Incorrect number", "You entered an incorrect number.", "Can't change the value.");
					return 0;
				}
			}
		}));
		pagesCountColumn.setOnEditCommit(x -> {
			Book selectedBook = bookTV.getSelectionModel().getSelectedItem();
			selectedBook.setPageCount(x.getNewValue());
		});

		statusColumn.setCellFactory(ComboBoxTableCell.forTableColumn(Status.observableStatusNames()));
		statusColumn.setOnEditCommit(statusName -> {
			Book selectedBook = bookTV.getSelectionModel().getSelectedItem();
			selectedBook.setStatus(Status.fromString(statusName.getNewValue()));
		});

		languagesColumn.setCellFactory(ComboBoxTableCell.forTableColumn(olc.getLanguageNames()));
		languagesColumn.setOnEditCommit(language -> {
			Book selectedBook = bookTV.getSelectionModel().getSelectedItem();
			selectedBook.setLanguage(new Language(language.getNewValue()));
		});

		setContextMenu(olc);
		setStyles(titleColumn, authorNameColumn, authorSurnameColumn);
	}

	private void setStyles(TableColumn<Book, String> titleColumn, TableColumn<Book, String> authorNameColumn,
			TableColumn<Book, String> authorSurnameColumn) {
		titleColumn.setStyle("-fx-font-weight: bold");
		authorNameColumn.setStyle("-fx-font-style: italic");
		authorSurnameColumn.setStyle("-fx-font-style: italic");
	}

	public void fetchData() {
		bookTV.setItems(sortedData);
	}

	public void setContextMenu(ObservableListController olc) {
		bookTV.setRowFactory(tableView -> {
			TableRow<Book> row = new TableRow<>();
			row.setOnMouseClicked(e -> {
				if (!row.isEmpty()) {
					Book selectedBook = row.getItem();
					ContextMenu contextMenu = new ContextMenu();
					MenuItem delete = new MenuItem("Delete Book");
					contextMenu.getItems().add(delete);
					bookTV.setContextMenu(contextMenu);
					delete.setOnAction(a -> {
						olc.delete(selectedBook);
					});
				} else {
					ContextMenu contextMenu = new ContextMenu();
					MenuItem add = new MenuItem("Add New Book");
					contextMenu.getItems().addAll(add);
					bookTV.setContextMenu(contextMenu);
					add.setOnAction(a -> {
						Optional<Book> optionalAddedBook = DialogBuilder.getBookFromDialog(olc);
						if (optionalAddedBook.isPresent()) {
							olc.add(optionalAddedBook.get());
						}
					});
				}
			});
			return row;
		});
	}

}
