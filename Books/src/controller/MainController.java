package controller;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import exceptions.DBException;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import model.Book;
import model.Language;
import model.Status;
import report.Reporter;
import utils.DialogBuilder;

public class MainController implements Initializable {

	@FXML
	private TableView<Book> booksTV;

	@FXML
	private TextField titleTF;

	@FXML
	private TextField authorNameTF;

	@FXML
	private TextField authorSurnameTF;

	@FXML
	private ComboBox<String> languageCB;

	@FXML
	private ComboBox<String> statusCB;

	@FXML
	private Button addBookBT;

	@FXML
	private Button addLanguageBT;
	
	@FXML
	private Button clearBT;

	@FXML
	private Button reportBT;
	

	private BookTableController btc;
	private ObservableListController olc;
	private final String PLACEHOLDER = "<empty>";

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		olc = new ObservableListController(PLACEHOLDER);
		try {
			olc.fetchFromDB();
		} catch (DBException e) {
			Platform.exit(); // data couldn't be read from the database, exit the program
		}
		btc = new BookTableController(booksTV, this, olc);
		btc.createColumns();
		btc.fetchData();

		filterInit();
		buttonInit(olc);
	}

	public void filterInit() {

		// setting up the language combo box:
		ObservableList<String> languageNamesWithPlaceHolder = olc.getLanguageNamesWithPlaceHolder();
		languageCB.setItems(languageNamesWithPlaceHolder);
		languageCB.getSelectionModel().select(PLACEHOLDER);

		// setting up the status combo box:
		ObservableList<String> statusChoice = Status.observableStatusNames();
		statusChoice.add(PLACEHOLDER);
		statusCB.setItems(statusChoice);
		statusCB.getSelectionModel().select(PLACEHOLDER);

		// registering the listeners on the filter components:
		listenToFilterChanges();

	}

	private void buttonInit(ObservableListController olc) {
		addBookBT.setOnAction(x -> {
			Optional<Book> book = DialogBuilder.getBookFromDialog(olc);
			if (book.isPresent()) {
				olc.add(book.get());
			}
		});
		addLanguageBT.setOnAction(x -> {
			Optional<Language> optionalLanguage = DialogBuilder.getLanguageFromDialog();
			if (optionalLanguage.isPresent()) {
				olc.add(optionalLanguage.get());
			}
		});
		
		clearBT.setOnAction(x-> {
			titleTF.setText(null);
			authorNameTF.setText(null);
			authorSurnameTF.setText(null);
			languageCB.setValue(PLACEHOLDER);
			statusCB.setValue(PLACEHOLDER);
		});
		
		reportBT.setOnAction(x -> Reporter.showReport(olc));
	}

	private void listenToFilterChanges() {
		listenAndFilter(titleTF.textProperty());
		listenAndFilter(authorNameTF.textProperty());
		listenAndFilter(authorSurnameTF.textProperty());
		listenAndFilter(languageCB.valueProperty());
		listenAndFilter(statusCB.valueProperty());
	}

	private <T> void listenAndFilter(ObservableValue<T> value) {
		value.addListener((observable, oldValue, newValue) -> {
			BookFilter bookFilter = new BookFilter(titleTF.getText(), authorNameTF.getText(), authorSurnameTF.getText(),
					languageCB.getValue(), statusCB.getValue());
			olc.filter(bookFilter, PLACEHOLDER);
		});
	}
}
