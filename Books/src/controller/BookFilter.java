package controller;

public class BookFilter {
	private String title;
	private String authorName;
	private String authorSurname;
	private String languageName;
	private String status;
	
	public BookFilter(String title, String authorName, String authorSurname, String languageName, String status) {
		this.title=title;
		this.authorName=authorName;
		this.authorSurname=authorSurname;
		this.languageName = languageName;
		this.status=status;
	}
	
	public String getTitle() {
		return title;
	}
	public String getAuthorName() {
		return authorName;
	}
	public String getAuthorSurname() {
		return authorSurname;
	}
	
	public String getLanguage() {
		return languageName;
	}
	
	public String getStatus() {
		return status;
	}
}
