package database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import exceptions.DBException;
import model.Author;
import model.Book;
import model.Language;
import model.Status;

public class BookDAO extends AbstractDAO {

	public Optional<Integer> find(Book book) throws DBException {
		
		String sql = "SELECT * FROM Book_tbl WHERE Title=? AND ID_Author =? AND ID_Language = ?";
		Optional<Integer> result = Optional.empty();
		AuthorDAO authorDAO = new AuthorDAO();
		int authorId = authorDAO.getId(book.getAuthor());
		LanguageDAO languageDAO = new LanguageDAO();
		int languageId = languageDAO.getId(book.getLanguage());
		try (DBAccess dbAccess = new DBAccess(); PreparedStatement ps = connection(dbAccess).prepareStatement(sql)) {
			ps.setString(1, book.getTitle());
			ps.setInt(2, authorId);
			ps.setInt(3, languageId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				int key = rs.getInt(1);
				result = Optional.of(key);
			}
		} catch (SQLException e) {
			throw new DBException("Error while reading the database.");
		}
		return result;
	}

	public void create(Book book) throws DBException {
		String sql = "INSERT INTO Book_tbl(title, ID_author, ID_language, pages_count, status) VALUES (?, ?, ?, ?, ?)";
		AuthorDAO authorDAO = new AuthorDAO();
		int authorId = authorDAO.getId(book.getAuthor());
		LanguageDAO languageDAO = new LanguageDAO();
		int languageId = languageDAO.getId(book.getLanguage());
		try (DBAccess dbAccess = new DBAccess(); PreparedStatement ps = connection(dbAccess).prepareStatement(sql)) {
			ps.setString(1, book.getTitle());
			ps.setInt(2, authorId);
			ps.setInt(3, languageId);
			ps.setInt(4, book.getPageCount());
			ps.setString(5, book.getStatus().getStatusName());
			if (!find(book).isPresent()) {
				int rows = ps.executeUpdate();
				if (rows == 0) {
					throw new DBException("Error while creating new Book record in the database.");
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Book read(int bookId) throws DBException {
		String sql = "SELECT * FROM Book_tbl WHERE Book_ID = ?";
		Book resultBook = null;
		try (DBAccess dbAccess = new DBAccess(); PreparedStatement ps = connection(dbAccess).prepareStatement(sql)) {
			ps.setInt(1, bookId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				resultBook = readBookFromResultSet(rs);
			}
		} catch (SQLException e) {
			throw new DBException("Error while reading the database.");
		}
		return resultBook;
	}

	private Book readBookFromResultSet(ResultSet rs) throws SQLException, DBException {
		String title = rs.getString("Title");
		int authorID = rs.getInt("ID_Author");
		int languageId = rs.getInt("ID_Language");
		int pagesCount = rs.getInt("Pages_Count");
		String statusName = rs.getString("Status");
		AuthorDAO authorDAO = new AuthorDAO();
		Author author = authorDAO.read(authorID);
		LanguageDAO languageDAO = new LanguageDAO();
		Language language = languageDAO.read(languageId);
		Status status = Status.fromString(statusName);
		Book resultBook = new Book(title, author, language, pagesCount, status);
		return resultBook;
	}

	public List<Book> readAll() throws DBException {
		String sql = "SELECT * FROM Book_tbl";
		List<Book> result = new ArrayList<>();
		try (DBAccess dbAccess = new DBAccess(); PreparedStatement ps = connection(dbAccess).prepareStatement(sql)) {
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Book resultBook = readBookFromResultSet(rs);
				result.add(resultBook);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Error while reading the database.");
		}
		return result;
	}
	
	public void update(Book oldBook, Book newBook) throws DBException {
		AuthorDAO authorDAO = new AuthorDAO();
		int newAuthorId = authorDAO.getId(newBook.getAuthor());
		LanguageDAO languageDAO = new LanguageDAO();
		int newLanguageId = languageDAO.getId(newBook.getLanguage());
		int bookId = getId(oldBook);
		String sql = "UPDATE Book_tbl SET title = ?, ID_author = ?, ID_language = ?, pages_count = ?, status = ? WHERE Book_ID = ?";
		try (DBAccess dbAccess = new DBAccess(); PreparedStatement ps = connection(dbAccess).prepareStatement(sql)) {
			ps.setString(1, newBook.getTitle());
			ps.setInt(2, newAuthorId);
			ps.setInt(3, newLanguageId);
			ps.setInt(4, newBook.getPageCount());
			ps.setString(5, newBook.getStatus().getStatusName());
			ps.setInt(6, bookId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DBException("Error while updating the database.");
		}
	}

	public void delete(Book book) throws DBException {
		String sql = "DELETE FROM Book_tbl WHERE Book_ID = ?";
		int bookId = getId(book);
		try (DBAccess dbAccess = new DBAccess(); PreparedStatement ps = connection(dbAccess).prepareStatement(sql)) {
			ps.setInt(1, bookId);
			int rows = ps.executeUpdate();
			if (rows == 0) {
			 throw new DBException("Error while deleting record from the database");	
			}
		} catch (SQLException e) {
			throw new DBException("Error while deleting record from the database.");
		}
	}

	public int getId(Book book) throws DBException {
		Optional<Integer> optionalBookId = find(book);
		if (optionalBookId.isPresent()) {
			return optionalBookId.get();
		}
		throw new DBException("Book not found");
	}
}
