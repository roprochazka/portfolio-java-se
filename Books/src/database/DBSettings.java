package database;

import java.util.Properties;

public class DBSettings {

	public static Properties load() {
		Properties p = new Properties();
		p.setProperty("url", "jdbc:ucanaccess://./Book DB/Book_DB.accdb");
		return p;
	}
}