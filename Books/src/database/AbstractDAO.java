package database;

import java.sql.Connection;

public abstract class AbstractDAO {

	protected Connection connection(DBAccess dbAccess) {
		return dbAccess.getConnection();
	}
}
