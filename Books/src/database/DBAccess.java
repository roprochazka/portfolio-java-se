package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import exceptions.DBException;

public class DBAccess implements AutoCloseable {
	private Connection connection;

	public DBAccess() throws DBException {
		Properties properties = DBSettings.load();
		try {
			connection = DriverManager.getConnection(properties.getProperty("url"), properties);

		} catch (SQLException e) {
			throw new DBException("Error while connecting to the database");
		}
	}

	public void close() throws DBException {
		try {
			if (connection != null)
				connection.close();
		} catch (SQLException e) {
			throw new DBException("Error while disconnecting from the database");
		}
	}

	public Connection getConnection() {
		return connection;
	}

}
