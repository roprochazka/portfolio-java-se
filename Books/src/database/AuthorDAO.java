package database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import exceptions.DBException;
import model.Author;

public class AuthorDAO extends AbstractDAO {

	/**
	 * 
	 * @param author
	 * @return the authorId if the author is found
	 * @throws DBException
	 */
	public Optional<Integer> find(Author author) throws DBException {
		final String sql = "SELECT * FROM Author_tbl WHERE Author_Name = ? AND Author_Surname = ?";
		Optional<Integer> result = Optional.empty();
		try (DBAccess dbAccess = new DBAccess(); PreparedStatement ps = connection(dbAccess).prepareStatement(sql);) {
			ps.setString(1, author.getAuthorName());
			ps.setString(2, author.getAuthorSurname());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				int key = rs.getInt(1);
				result = Optional.of(key);
			}
		} catch (SQLException e) {
			throw new DBException("Error reading databse.");
		}
		return result;
	}

	public void create(Author author) throws DBException {
		final String sql = "INSERT INTO Author_tbl(Author_name, Author_surname) VALUES (?, ?)";
		try (DBAccess dbAccess = new DBAccess(); PreparedStatement ps = connection(dbAccess).prepareStatement(sql);) {
			ps.setString(1, author.getAuthorName());
			ps.setString(2, author.getAuthorSurname());
			if (!find(author).isPresent()) { // the author does not already exists
				int rows = ps.executeUpdate();
				if (rows == 0) { // new rows were created
					throw new DBException("Error creating new record in database.");
				}
			}
		} catch (SQLException e) {
			throw new DBException("Error while creating a new record in the database.");
		}
	}

	public Author read(int authorId) throws DBException {
		Author resultAuthor = null;
		final String sql = "SELECT * FROM Author_tbl WHERE Author_ID = ?";
		try (DBAccess dbAccess = new DBAccess(); PreparedStatement ps = connection(dbAccess).prepareStatement(sql);) {
			ps.setInt(1, authorId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				String authorName = rs.getString("Author_name");
				String authorSurname = rs.getString("Author_surname");
				resultAuthor = new Author(authorName, authorSurname);
			}
		} catch (SQLException e) {
			throw new DBException("Error while reading database.");
		}
		return resultAuthor;
	}

	public List<Author> readAll() throws DBException {
		final String sql = "SELECT * FROM Author_tbl";
		List<Author> result = new ArrayList<>();
		try (DBAccess dbAccess = new DBAccess(); PreparedStatement ps = connection(dbAccess).prepareStatement(sql);) {
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				String authorName = rs.getString("Author_name");
				String authorSurname = rs.getString("Author_surname");
				result.add(new Author(authorName, authorSurname));
			}
		} catch (SQLException e) {
			throw new DBException("Error reading database.");
		}
		return result;
	}

	public void update(Author oldAuthor, Author newAuthor) throws DBException {
		final String sql = "UPDATE Author_tbl SET Author_name = ?, Author_surname = ? WHERE Author_ID = ?";
		int authorId = getId(oldAuthor);
		try (DBAccess dbAccess = new DBAccess(); PreparedStatement ps = connection(dbAccess).prepareStatement(sql);) {
			ps.setString(1, newAuthor.getAuthorName());
			ps.setString(2, newAuthor.getAuthorSurname());
			ps.setInt(3, authorId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DBException("Error while updating database.");
		}
	}

	public void delete(Author author) throws DBException {
		final String sql = "DELETE FROM Author_tbl WHERE Author_ID = ?";
		int authorId = getId(author);
		try (DBAccess dbAccess = new DBAccess(); PreparedStatement ps = connection(dbAccess).prepareStatement(sql);) {
			ps.setInt(1, authorId);
			int rows = ps.executeUpdate();
			if (rows == 0) {
				throw new DBException("Error while deleting from database " + author);
			}
		} catch (SQLException e) {
			throw new DBException("Error while deleting from database: " + author);
		}
	}

	public int getId(Author author) throws DBException {
		Optional<Integer> optionalAuthorId = find(author);
		if (optionalAuthorId.isPresent()) {
			return optionalAuthorId.get();
		}
		throw new DBException("Error while deleting from database: " + author);
	}
}
