package database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import exceptions.DBException;
import model.Language;

public class LanguageDAO extends AbstractDAO {

	public Optional<Integer> find(Language language) throws DBException {
		final String sql = "SELECT * FROM Language_tbl WHERE Language_Name = ?";
		Optional<Integer> result = Optional.empty();
		try (DBAccess dbAccess = new DBAccess(); PreparedStatement ps = connection(dbAccess).prepareStatement(sql);) {
			ps.setString(1, language.getLanguageName());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				int key = rs.getInt(1);
				result = Optional.of(key);
			}
		} catch (SQLException e) {
			throw new DBException("Error reading database.");
		}
		return result;
	}

	public void create(Language language) throws DBException {
		final String sql = "INSERT INTO Language_tbl(Language_name) VALUES (?)";
		try (DBAccess dbAccess = new DBAccess(); PreparedStatement ps = connection(dbAccess).prepareStatement(sql);) {
			ps.setString(1, language.getLanguageName());
			if (!find(language).isPresent()) { // the language does not already exists
				int rows = ps.executeUpdate();
				if (rows == 0) { // new rows were created
					throw new DBException("Error creating new record in database.");
				}
			}
		} catch (SQLException e) {
			throw new DBException("Error while creating a new record in the database.");
		}
	}

	public Language read(int languageId) throws DBException {
		Language resultLanguage = null;
		final String sql = "SELECT * FROM Language_tbl WHERE Language_ID = ?";
		try (DBAccess dbAccess = new DBAccess(); PreparedStatement ps = connection(dbAccess).prepareStatement(sql);) {
			ps.setInt(1, languageId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				String languageName = rs.getString("Language_name");
				resultLanguage = new Language(languageName);
			}
		} catch (SQLException e) {
			throw new DBException("Error while reading database.");
		}
		return resultLanguage;
	}

	public List<Language> readAll() throws DBException {
		final String sql = "SELECT * FROM Language_tbl";
		List<Language> result = new ArrayList<>();
		try (DBAccess dbAccess = new DBAccess(); PreparedStatement ps = connection(dbAccess).prepareStatement(sql);) {
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				String languageName = rs.getString("Language_name");
				result.add(new Language(languageName));
			}
		} catch (SQLException e) {
			throw new DBException("Error reading database.");
		}
		return result;
	}

	public void update(Language oldLanguage, Language newLanguage) throws DBException {
		final String sql = "UPDATE Language_tbl SET Language_name = ? WHERE Language_ID = ?";
		int languageId = getId(oldLanguage);
		try (DBAccess dbAccess = new DBAccess(); PreparedStatement ps = connection(dbAccess).prepareStatement(sql);) {
			ps.setString(1, newLanguage.getLanguageName());
			ps.setInt(3, languageId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DBException("Error while updating database.");
		}
	}

	public void delete(Language language) throws DBException {
		final String sql = "DELETE FROM Language_tbl WHERE Language_ID = ?";
		int languageId = getId(language);
		try (DBAccess dbAccess = new DBAccess(); PreparedStatement ps = connection(dbAccess).prepareStatement(sql);) {
			ps.setInt(1, languageId);
			int rows = ps.executeUpdate();
			if (rows == 0) {
				throw new DBException("Error while deleting from database");
			}
			;
		} catch (SQLException e) {
			throw new DBException("Error while deleting from database");
		}
	}

	public int getId(Language language) throws DBException {
		Optional<Integer> optionalLanguageId = find(language);
		if (optionalLanguageId.isPresent()) {
			return optionalLanguageId.get();
		}
		throw new DBException("Error while deleting from database.");
	}
}
