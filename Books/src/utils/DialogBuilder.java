package utils;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import controller.ObservableListController;
import javafx.beans.binding.BooleanBinding;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import model.Author;
import model.Book;
import model.Language;
import model.Status;
import report.ReportType;

public class DialogBuilder {

	public static Optional<Book> getBookFromDialog(ObservableListController olc) {

		Dialog<Book> dialog = new Dialog<>();
		dialog.setTitle("Add new book");
		dialog.setHeaderText("Enter the book data:");

		ImageView dialogImage = new ImageView("reading.png");
		dialog.setGraphic(dialogImage);

		Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
		stage.getIcons().add(new Image("open-book.png"));
		dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

		GridPane gridPane = new GridPane();
		gridPane.setHgap(10);
		gridPane.setVgap(10);
		gridPane.setPadding(new Insets(20, 150, 10, 10));

		TextField titleTF = new TextField();
		titleTF.setPromptText("Title");
		gridPane.add(new Label("Title:"), 0, 0);
		gridPane.add(titleTF, 1, 0);

		TextField authorNameTF = new TextField();
		authorNameTF.setPromptText("Author Name");
		gridPane.add(new Label("Author Name:"), 0, 1);
		gridPane.add(authorNameTF, 1, 1);

		TextField authorSurnameTF = new TextField();
		authorSurnameTF.setPromptText("Author Surname");
		gridPane.add(new Label("Author Surname:"), 0, 2);
		gridPane.add(authorSurnameTF, 1, 2);

		ComboBox<String> languageCB = new ComboBox<>();
		languageCB.setItems(olc.getLanguageNames());
		languageCB.setPromptText("Language");
		gridPane.add(new Label("Language:"), 0, 3);
		gridPane.add(languageCB, 1, 3);

		TextField pagesTF = new TextField();
		validateInputAsInteger(pagesTF);
		pagesTF.setPromptText("Max 999 999 999.");
		gridPane.add(new Label("Pages:"), 0, 4);
		gridPane.add(pagesTF, 1, 4);

		ComboBox<String> statusCB = new ComboBox<>();
		statusCB.setPromptText("Status");
		statusCB.setItems(Status.observableStatusNames());
		gridPane.add(new Label("Status:"), 0, 5);
		gridPane.add(statusCB, 1, 5);

		dialog.getDialogPane().setContent(gridPane);
		dialog.getDialogPane().lookupButton(ButtonType.OK).setDisable(true);

		// validate dialog - allow ok button only if all fields are valid
		BooleanBinding titleValid = titleTF.textProperty().isNotEmpty();
		BooleanBinding authorNameValid = authorNameTF.textProperty().isNotEmpty();
		BooleanBinding authorSurnameValid = authorSurnameTF.textProperty().isNotEmpty();
		BooleanBinding languageChosen = languageCB.valueProperty().isNotNull();
		BooleanBinding pagesValid = pagesTF.textProperty().isNotEmpty();
		BooleanBinding statusChosen = statusCB.valueProperty().isNotNull();
		BooleanBinding dialogValid = titleValid.and(authorNameValid).and(authorSurnameValid).and(languageChosen)
				.and(pagesValid).and(statusChosen);
		BooleanBinding dialogInvalid = dialogValid.not();
		Node okButton = dialog.getDialogPane().lookupButton(ButtonType.OK);
		okButton.disableProperty().bind(dialogInvalid);

		dialog.setResultConverter(dialogButton -> {
			if (dialogButton == ButtonType.OK) {
				String title = titleTF.getText();
				Author author = new Author(authorNameTF.getText(), authorSurnameTF.getText());
				Language language = new Language(languageCB.getValue());
				int pages = 0;
				pages = Integer.parseInt(pagesTF.getText());
				Status status = Status.fromString(statusCB.getValue());
				return new Book(title, author, language, pages, status);
			}
			return null;
		});

		return dialog.showAndWait();
	}

	private static void validateInputAsInteger(TextField textField) {
		textField.focusedProperty().addListener((observable, oldValue, newValue) -> {
			if (!newValue && !textField.getText().matches("[0-9]{1,9}")) {
				textField.requestFocus();
			}
		});
	}

	public static Optional<Language> getLanguageFromDialog() {
		TextInputDialog languageDialog = new TextInputDialog();
		Stage stage = (Stage) languageDialog.getDialogPane().getScene().getWindow();
		stage.getIcons().add(new Image("open-book.png"));
		languageDialog.setTitle("Add New Language");
		languageDialog.setHeaderText("Enter the name of the new language:");
		languageDialog.setContentText("Language name:");
		ImageView alertImage = new ImageView("language.png");
		languageDialog.setGraphic(alertImage);

		// validate the dialog:
		Node okButton = languageDialog.getDialogPane().lookupButton(ButtonType.OK);
		okButton.disableProperty().bind(languageDialog.getEditor().textProperty().isEmpty());

		Optional<String> result = languageDialog.showAndWait();
		if (result.isPresent()) {
			return Optional.of(new Language(result.get()));
		}
		return Optional.empty();

	}

	public static void getAlert(String title, String headerText, String text) {
		Alert alert = new Alert(AlertType.NONE, text, ButtonType.OK);
		Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
		stage.getIcons().add(new Image("open-book.png"));
		alert.setTitle(title);
		alert.setHeaderText(headerText);
		ImageView alertImage = new ImageView("exclamation.png");
		alert.setGraphic(alertImage);
		alert.showAndWait();
	}

	public static Optional<ReportType> getReportTypeFromDialog() {
		List<String> choices = Arrays.asList(ReportType.values()).stream().map(x -> x.getFullReportName())
				.collect(Collectors.toList());
		String defaultChoice = ReportType.PIE_PAGES_READ.getFullReportName();
		ChoiceDialog<String> choiceDialog = new ChoiceDialog<>(defaultChoice, choices);
		Stage stage = (Stage) choiceDialog.getDialogPane().getScene().getWindow();
		stage.getIcons().add(new Image("open-book.png"));
		choiceDialog.setTitle("Generate Report");
		choiceDialog.setHeaderText("Choose a report to be generated:");
		choiceDialog.setContentText("Report:");
		ImageView dialogImage = new ImageView("reading.png");
		choiceDialog.setGraphic(dialogImage);
		Optional<String> dialogResult = choiceDialog.showAndWait();
		if (dialogResult.isPresent()) {
			return Optional.of(ReportType.fromString(dialogResult.get()));
		} 
		return Optional.empty();
		
	}
}
