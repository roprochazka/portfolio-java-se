package test;

import database.AuthorDAO;
import database.BookDAO;
import database.LanguageDAO;
import exceptions.DBException;

public class Test {
	// ************************* TEST *****************************
	public static void main(String[] args) throws DBException {
		LanguageDAO langDAO = new LanguageDAO();
		AuthorDAO authorDAO = new AuthorDAO();
		BookDAO bookDAO = new BookDAO();
		System.out.println(langDAO.read(1));
		System.out.println(authorDAO.read(55));
		System.out.println(bookDAO.read(50));
	}

}
